<?php
use CoffeeCode\Router\Router;
use Source\Helpers\TokenIsValid;

    ob_start();
    require 'vendor/autoload.php';
    session_start();

    $token = new TokenIsValid();

    $route = new Router(ROOT);
    $origyn = $_SERVER['REQUEST_SCHEME']."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

    if(!$token->testTokenValidate() && $origyn !== ROOT .'/' && strpos($origyn, 'login') === false && strpos($origyn, 'register') === false) {
        header("Location: " . ROOT);
        die;
    };

    /**
     * APP
     */
    $route->namespace("Source\App");

    $route->group('app');
    $route->get("/login", "Web:applicationLogin");
    $route->post("/register", "Web:applicationLoginRegister");

    //Verifica se existe a rotazione
    if( (!isset($_GET['route']) OR strpos($_GET['route'], 'app/') === false) AND !isset($_SESSION['app_logged']) ) {
        header("Location: " . ROOT . '/app/login');
        die();
    } elseif(isset($_GET['route']) && strpos($_GET['route'], 'app/') && isset($_SESSION['app_logged'])) {
        header("Location: " . ROOT);
    }

    /**
     * WEB
     */
    $route->group(null);
    $route->get("/", "Web:home");
    $route->get("/login", "Web:login");
    $route->get("/listAccounts", "Web:getAccounts");
    $route->get("/cron/accounts/{account}/location/{location}/clone", "Web:cronRepeaterPost");
    $route->get("/cron/accounts/{account}/localPost/clone", "Web:cronRepeaterAllLocationPost");


    $route->get("/test", "WebLocalPost:createMultiplesPost");
    $route->post("/mail/send", "Web:sendMail");

    /**
     * Web Location
    */
    $route->get("/accounts/{account}/locations", "Location:accountLocations", "show.locations");
    $route->get("/account/{account}/locations", "Location:getAllLocations", "list.locations");
    $route->get("/account/{account}/locationsList", "Web:getLocationsList");
    $route->get("/accounts/{account}/location/{location}/services", "Web:getLocaleService");

    /**
     * Web Local Post
     */
    $route->get("/accounts/{account}/location/{location}/localPost", "WebLocalPost:getLocalPost");
    $route->get("/accounts/{account}/location/{location}/localPost/{localPostId}", "WebLocalPost:getLocalPost");
    $route->get("/accounts/{account}/locations/localPost", "WebLocalPost:getAllLocalPostForAccount");
    $route->post("/accounts/locations/localPostListRelauch", "WebLocalPost:relaunchPostsAccountForLocations");

    $route->delete("/localpost/delete", "WebLocalPost:deleteLocalPost");
    $route->post("/accounts/{account}/location/{location}/localPosts/delete", "WebLocalPost:deleteMultiplesLocalPost");

    $route->post("/accounts/localPost/clone", "WebLocalPost:cloneAllLocalPostForAccount");
    $route->post("/location/localPost/clone", "WebLocalPost:cloneAllLocalPostForLocation");
    $route->post("/localPost/clone", "WebLocalPost:clonePost");
    $route->post("/localPost/relaunch", "WebLocalPost:clonePostV2");
    $route->post("/localPost/newAlert", "WebLocalPost:createLocalPost");

    $route->post("/localPost/create", "WebLocalPost:createLocalPost");
    $route->post("/accounts/{account}/location/{location}/localPost/createNew", "WebLocalPost:createLocalPostNew");
    $route->post("/accounts/{account}/location/localPost/multiples", "WebLocalPost:createMultiplesPost");
    $route->post("/accounts/{account}/location/{location}/localPost/{localPostId}/metrics", "WebLocalPost:getMetrics");

    $route->post("/getJson", "WebLocalPost:showArray");

    /**
     * Review
     */
    $route->get("/accounts/{account}/location/{location}/reviews", "WebReviews:getReviews");

    /**
     * Google Services
     */
    $route->get("/drive", "WebGoogleServices:listFiles");
    $route->get("/sheet/{id}", "WebGoogleServices:getSheetValues");
    $route->post("/accounts/{account}/sheet/{sheet_id}", "WebGoogleServices:ShowSheetValues");
    $route->post("/accounts/{account}/location/{location}/sheet/{sheet_id}", "WebGoogleServices:ShowSheetValues");

    /**
     * Error
     */
    $route->group('ooops');
    $route->get("/{errcode}", "Web:error");


    //Admin area
    /**
     * ADMIN
     */
    $route->namespace("Source\Admin");

    /**
     * Admin Area
     */
    $route->group('admin');
    $route->get("/", "AdminArea:home");
    $route->get("/accounts", "AdminAccounts:getAccounts", "admin.accounts");
    $route->get("/locations", "AdminLocations:getLocations", "admin.locations");
$route->get("/config", "AdminConfigs:getConfigs", "admin.settings");
    $route->post("/logs", "AdminArea:getLogs");
    $route->post("/logscsv", "AdminArea:createCsv");

    /**
     * Process
     */
    $route->dispatch();

    if($route->error()) {
        $route->redirect("/ooops/{$route->error()}");
    }

    ob_end_flush();