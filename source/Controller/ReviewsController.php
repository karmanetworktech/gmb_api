<?php


namespace Source\Controller;

use Source\Models\Reviews;


class ReviewsController
{
    public Reviews $reviews;

    /**
     * ReviewsController constructor.
     */
    public function __construct()
    {
        $this->reviews = new Reviews();
    }

    public function listReviews($account, $location): object {

        if($account && $location) {
            $this->reviews->account_id = $account;
            $this->reviews->location_id= $location;

            return json_decode( $this->reviews->getReviews() );
        } else {
            $output = array(
                'code' => 400,
                'msg' => 'The account and location are must informed!'
            );
            return json_decode(json_encode($output));
        }

    }
}