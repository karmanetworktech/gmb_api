<?php


namespace Source\Controller;
use Source\Helpers\LocalPostsHelper;
use Source\Models\LocalPosts;

class LocalPostController
{
    private LocalPosts $localPost;
    private LocalPostsHelper $localPostsHelper;
    private LogController $logger;

    public function __construct()
    {
        $this->localPost = new LocalPosts();
        $this->localPostsHelper = new LocalPostsHelper();
        $this->logger = new LogController();
    }

    public function getLocalPostLocation($account, $location, $localPostId = null)
    {
        $this->localPost->accountId = $account;
        $this->localPost->locationId = $location;
        if($localPostId) { $this->localPost->localPostId = $localPostId; }
        else { $this->localPost->localPostId = ""; }

        return $this->localPost->getLocalPostFromLocation();
    }

    public function createLocalPost($data, $account, $location)
    {
        $this->localPost->accountId = $account;
        $this->localPost->locationId = $location;

        $new_local_post = $this->localPost->createLocalPost($data);

        $logger_data = array(
            'user'          => $_SESSION['client_info']['name'],
            'action'        => 'Create a new local post',
            'type_action'   => 'create_local_post',
            'account'       => $account,
            'location'      => $location
        );

        //$this->logger->registerlog($logger_data);

        return $new_local_post;
    }

    public function newCreateLocalPost($data, $account, $location)
    {
        $this->localPost->accountId = $account;
        $this->localPost->locationId = $location;

        $new_local_post = $this->localPost->newCreateLocalPost($data);

        $logger_data = array(
            'user'          => $_SESSION['client_info']['name'],
            'action'        => 'Create a new local post',
            'type_action'   => 'create_local_post',
            'account'       => $account,
            'location'      => $location
        );

        //$this->logger->registerlog($logger_data);

        return $new_local_post;
    }

    public function deleteLocalPost($account, $location, $postId)
    {
        $this->localPost->accountId   = $account;
        $this->localPost->locationId  = $location;
        $this->localPost->localPostId = $postId;

        return $this->localPost->removeLocalPost();
    }

    public function repeatPost($account, $location, $post_data): array
    {
        $output = [];
        try {
            $createPost = json_decode($this->newCreateLocalPost($post_data, $account, $location));
            $postIdNew = explode("/", $createPost->name)[sizeof(explode("/", $createPost->name)) - 1];

            if ($postIdNew) {
                array_push($output, array(
                    "code" => '200',  "msg" => "Post has cloned successful!"
                ));
            } else {
                array_push($output, [
                    "code" => '500', "msg" => "Error to create the post, error {$createPost->message}!"
                ]);
            }

        } catch (\Throwable $th) {
            array_push($output, [
                "code" => '500', "msg" => "Error to create the post, error {$th->getMessage()}!"
            ]);
        }

        return $output;
    }

    public function repeatPostV2($account, $location, $post_data): array
    {
        $this->localPostsHelper->localPostData = $post_data;
        $postData = $this->localPostsHelper->generateJsonLocalPostData();

        $output = [];
        $postIdOld = explode("/", $post_data->name)[sizeof(explode("/", $post_data->name)) - 1];
        try {
            $createPost = json_decode($this->createLocalPost($postData, $account, $location));
            $postIdNew = explode("/", $createPost->name)[sizeof(explode("/", $createPost->name)) - 1];

            //$this->deleteLocalPost($account, $location, $postIdOld);

            if ($postIdNew) {
                array_push($output, [
                    "code" => 200,
                    "msg" => "Post {$postIdOld} has cloned successful!\n"
                ]);
            } else {
                array_push($output, [
                    "code" => 500,
                    "msg" => "Error to create the post {$postIdOld}, error {$createPost->message}!"
                ]);
            }

        } catch (\Throwable $th) {
            array_push($output, [
                "code" => 500,
                "msg" => "Error to create the post {$postIdOld}, error: {$th->getMessage()}!"
            ]);
        }

        return $output;
    }

    public function listAllLocalPostForAccount($locationList, $account): array
    {
        $final_array = array();

        if(!$locationList['locations']) {
            return ['error' => "This account don't have any locations."];
        }
        
        foreach ($locationList['locations'] as $key => $location) {
            $location_info      = explode('/', $location->name);
            $location_status    = $location->openInfo->status;
            $location_locale    = $location_info[1];

            if($location_status == 'OPEN' ) {
                $this->localPost->accountId = $account;
                $this->localPost->locationId = $location_locale;
                $this->localPost->localPostId = "";

                $localPost_list =  json_decode($this->localPost->getLocalPostFromLocation());
                
                if(count((array)$localPost_list) > 0) {
                    $location_data = array(
                        'name'      => $location->title ,
                        'account'   => $account,
                        'location'  => $location_locale,
                        'local_post'=> $localPost_list
                    );

                    array_push($final_array,$location_data);
                } else {
                    $location_data = array(
                        'name'      => $location->title,
                        'account'   => $account,
                        'location'  => $location_locale,
                        'local_post'=> 'Nenhum post encontrado!'
                    );

                    array_push($final_array,$location_data);
                }
                
            }
        }

        return $final_array;
    }

    public function listMetrics($account, $location, $post_id)
    {
        $this->localPost->accountId  = $account;
        $this->localPost->locationId = $location;
        $this->localPost->locationId = $post_id;

        return $this->localPost->listMetrics(null);
    }
}