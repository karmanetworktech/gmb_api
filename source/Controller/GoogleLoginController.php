<?php
namespace Source\Controller;

require_once "./vendor/autoload.php";

use DateInterval;
use DateTime;
use Google_Client;
use Source\Controller\LogController;

class GoogleLoginController
{
    private Google_Client $googleClient;

    public function __construct()
    {
        $this->googleClient = new Google_Client();
        $this->googleClient->setApplicationName("Google My Business API Manager");
        $this->googleClient->setClientId(GOOGLE['clientId']);
        $this->googleClient->setClientSecret(GOOGLE['clientSecret']);
        $this->googleClient->setRedirectUri(GOOGLE['redirectUri'].'/login');
        $this->googleClient->addScope(GOOGLE['scope']);
    }

    public function getAuthorizationUrl(): string
    {
        return $this->googleClient->createAuthUrl();
    }

    public function getToken($code): array
    {
        $output = $this->googleClient->fetchAccessTokenWithAuthCode($code);

        $date = null;
        try {
            date_default_timezone_set('Europe/Lisbon');
            $date = new DateTime(date('Y-m-d H:i:s'));
            $date->add(new DateInterval('PT3400S'));
        } catch (\Exception $e) {
            echo $e->getMessage();
            die;
        }

        $_SESSION['time_to_refresh'] = $date->format('H:i:s');

        //Client data
        $oauth = new \Google_Service_Oauth2($this->googleClient);
        $_SESSION['client_info'] = $oauth->userinfo->get();

        return $output;
    }

    /**
     * @return void
     */
    public function logout(): void
    {
        unset($_SESSION['google_token']);
        unset($_SESSION['client_info']);
        unset($_SESSION['time_to_refresh']);
    }

    /**
     * @return Google_Client
     */
    public function getGoogleClient(): Google_Client
    {
        return $this->googleClient;
    }

    public function refreshToken($token)
    {
        $this->googleclient->refreshToken($token);
        $_SESSION['tokenInfo'] = $this->googleClient->tokeninfo(['access_token' => $token]);

        var_dump($_SESSION['tokenInfo']);
    }
}