<?php

namespace Source\Controller\Admin;

use Source\Models\Admin\Locations;

class LocationsControllerAdmin
{
    protected Locations $locations;

    /**
     * LogController constructor.
     */
    public function __construct()
    {
        $this->locations = new Locations();
    }

    public function getLocations()
    {
        return $this->locations->loadLocations();
    }
}