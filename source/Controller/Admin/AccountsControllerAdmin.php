<?php

namespace Source\Controller\Admin;

use Source\Models\Admin\Accounts;

class AccountsControllerAdmin
{
    protected Accounts $accounts;

    /**
     * LogController constructor.
     */
    public function __construct()
    {
        $this->accounts = new Accounts();
    }

    public function getAccounts()
    {
        return $this->accounts->loadAccounts();
    }
}