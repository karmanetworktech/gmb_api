<?php

namespace Source\Controller\Admin;

use Source\Models\Admin\Logs;

class LogsController
{
    protected Logs $logs;

    /**
     * LogController constructor.
     */
    public function __construct()
    {
        $this->logs         = new Logs();
        $this->itensLimit   = 10;
        $this->logType      = 'all';
        $this->orderBy      = 'date';
        $this->order        = 'ASC';
        $this->offset       = 0;
        $this->dateInit     = null;
        $this->dateFinal    = null;
    }

    public function getLogs()
    {

        $filter_columns = '';
        $filter_rows = '';
        
        /** ============================== FILTER PARAMS ================================= */
        $params_columns = $this->logType !== 'all' ? "type_action = :type_action" : '';
        $params_rows    = $this->logType !== 'all' ? "type_action={$this->logType}" : '';
        if($params_columns != '') {
            $filter_columns = $params_columns != '' ? $params_columns : $filter_columns;
            $filter_rows    = $params_rows != '' ? $params_rows : $params_rows;
        }
        /** ============================== FILTER PARAMS ================================= */
        
        /** ============================== DATE INIT ================================= */
        $start_date_columns = $this->dateInit ? "date >= :date_start" : '';
        $start_date_rows    = $this->dateInit ? "date_start=$this->dateInit 00:00:00" : '';

        if($start_date_columns != '') {
            $filter_columns = $filter_columns != '' ? $filter_columns .' AND '.$start_date_columns : $start_date_columns;
            $filter_rows = $filter_rows != '' ? $filter_rows .'&'.$start_date_rows : $start_date_rows;
        }
        /** ============================== DATE INIT ================================= */


        /** ============================== DATE FINISH ================================= */
        $final_date_columns = $this->dateFinal ? "date <= :date_final" : '';
        $final_date_rows    = $this->dateFinal ? "date_final=$this->dateFinal 23:59:59" : '';

        if($final_date_columns != '') {
            $filter_columns = $filter_columns != '' ? $filter_columns .' AND '.$final_date_columns : $final_date_columns;
            $filter_rows = $filter_rows != '' ? $filter_rows .'&'.$final_date_rows : $final_date_rows;
        }
        /** ============================== DATE FINISH ================================= */

        return $this->logs->LoadLogs($this->itensLimit, $filter_columns, $filter_rows, $this->orderBy, $this->order, $this->offset);
    }
}