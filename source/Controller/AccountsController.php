<?php


namespace Source\Controller;
use Source\Models\Accounts;


class AccountsController
{
    public function listAllAccounts()
    {
        $accounts = new Accounts();
        return $accounts->listAllAccounts();
    }


    public function accountLocations($account)
    {
        $accounts = new Accounts();
        $accounts->accountId = $account;
        return $accounts->listAllLocations();
    }

    public function accountInfo($account)
    {
        $accounts = new Accounts();
        $accounts->accountId = $account;
        return $accounts->getAccountInfo();
    }

    public function getAccoutName($id) {
        $accounts = new Accounts();

        return $accounts->accountInfo($id);
    }
}