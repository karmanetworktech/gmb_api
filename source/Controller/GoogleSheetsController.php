<?php


namespace Source\Controller;
use Source\Models\GoogleSheets;

class GoogleSheetsController
{
    protected GoogleSheets $gSheets;
    public $id;

    /**
     * GoogleSheetsController constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->gSheets = new GoogleSheets;
        $this->id = $id;
    }

    public function getValues()
    {
        if($this->id) {
            $this->gSheets->id = $this->id;
            return $this->gSheets->getValues();
        }
    }

    public function formatValues($data, $list_index, $filters): array
    {
        $new_object = [];
        $size_data = sizeof($data);
        $today = date('n/j/Y');

        for ($index = 1; $index < $size_data; $index++) {

            $_data = array(
                "languageCode"  => 'en-US',
                "summary"       => $data[$index][$list_index['summary']],
                "callToAction"  => array(
                    "actionType"=> 'LEARN_MORE',
                    "url"       => $data[$index][$list_index['action_url']]
                ),
                "topicType"     => 'STANDARD',
                "media"         => array(
                    "mediaFormat"   => "PHOTO",
                    "sourceUrl"     => $data[$index][$list_index['img_url']]
                )
            );

            if($filters == 'all') {
                array_push($new_object, $_data);
            } else {
                $sheetDate = $data[$index][$list_index['filter_date']];
                if($sheetDate === $today) {
                    array_push($new_object, $_data);
                }
            }
        }
        return $new_object;
    }
}