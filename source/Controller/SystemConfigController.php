<?php

namespace Source\Controller;

use Source\Models\SystemConfig;

class SystemConfigController
{
    private SystemConfig $configs;

    public function __construct()
    {
        $this->configs = new SystemConfig();
    }

    /**
     * @return SystemConfig
     */
    public function listConfigs(): SystemConfig
    {
        return $this->configs->getConfigs();
    }

    public function setConfigs()
    {
        return $this->configs->setConfigs();
    }
}