<?php


namespace Source\Controller;
use Source\Models\GoogleDrive;

class GoogleDriveController
{
    private GoogleDrive $google_drive;
    /**
     * GoogleDriveController constructor.
     */
    public function __construct()
    {
        $this->google_drive = new GoogleDrive;
    }

    public function listAllFiles()
    {
        return json_decode($this->google_drive->fileList());
    }
}