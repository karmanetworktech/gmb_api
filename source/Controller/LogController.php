<?php


namespace Source\Controller;
use Source\Models\Log;

class LogController
{

    protected Log $log;

    /**
     * LogController constructor.
     */
    public function __construct()
    {
        $this->log = new Log();
    }

    public function registerlog($data)
    {
        if( !isset($data['user']) ) {
            return array('code' => 400, 'msg' => 'The name must informed');
        }

        if( !isset($data['action']) ) {
            return array('code' => 400, 'msg' => 'The action must informed');
        }

        if( !isset($data['type_action']) ) {
            return array('code' => 400, 'msg' => 'The type action must informed');
        }

        $this->log->addLog($data);
    }

    public function listLogs()
    {
        $logs = $this->log;
        return $logs->find()->fetch(true);
    }
}