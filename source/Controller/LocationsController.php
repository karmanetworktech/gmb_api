<?php

namespace Source\Controller;
use Source\Models\Locations;

class LocationsController
{

    public function __construct($var = null) {
        $this->locationController = new Locations();
    }

    public function listAllLocations($account, $pageToken = null, $locationsPerPage = 100)
    {
        $locations = new Locations();
        $locations->accountId   = $account;
        $locations->page_token  = $pageToken;
        $locations->qtd_per_page= $locationsPerPage;

        $list_locations = json_decode($locations->listAllLocations());

        $all_locations = isset($list_locations->locations) ? $list_locations->locations : null;
        $next_page_token = isset($list_locations->nextPageToken) ? $list_locations->nextPageToken : null;

        $output = [];

        if($next_page_token) {
            $output = array('locations' => $all_locations, 'next_page' => $next_page_token);
        }
        else {
            $output = array('locations' => $all_locations, 'next_page' => null);
        }

        return $output;
    }

    public function getLocation($account, $location)
    {
        $location_info = new Locations();
        $location_info->accountId   = $account;
        $location_info->locationId  = $location;

        return $location_info->getLocation();
    }

    public function getLocationInfo($account, $location)
    {
        $location_info = new Locations();
        $location_info->accountId   = $account;
        $location_info->locationId  = $location;

        return $location_info->locationInfo();
    }

    public function getServicesLocation($account, $location)
    {
        $this->locationController->accountId   = $account;
        $this->locationController->locationId  = $location;

        return $this->locationController->getServices();
    }

    public function registerLocation($account, $location) {
        $this->locationController->accountId = $account;
        $this->locationController->locationId = $location;
        
        $this->locationController->locationInfo();
    }
}