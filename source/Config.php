<?php

    const ROOT = "http://localhost/gmb_api";
    const SITE = "Google my business - API";

    const GMB_DEFAULT_LINK      = 'https://mybusiness.googleapis.com/v4';
    const GMB_ACCOUNTS_LINK     = 'https://mybusinessaccountmanagement.googleapis.com/v1';
    const DRIVE_DEFAULT_LINK    = 'https://www.googleapis.com/drive/v3';
    const SHEETS_DEFAULT_LINK   = 'https://sheets.googleapis.com/v4';

    const GMB_API_LINKS = array(
        'accounts' => 'https://mybusinessaccountmanagement.googleapis.com/v1',
        'locations'=> 'https://mybusinessbusinessinformation.googleapis.com/v1'
    );

    const CONFIG_SMTP_MAIL = [
        'host'      => 'smtp.gmail.com',
        'port'      => '465',
        'user'      => 'karmamybusiness@gmail.com',
        'pass'      => 'Liberdade21.karma',
        'fromName'  => 'Karma My Business',
        'fromMail'  => 'karmamybusiness@gmail.com',
    ];

    const GOOGLE = [
        'clientId'      => '716310763984-23kfujqcpsmt02c1lp4pkhudr2ulk5rv.apps.googleusercontent.com',
        'clientSecret'  => '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',
        'redirectUri'   => ROOT,
        'scope'         => [
            'https://www.googleapis.com/auth/plus.business.manage',
            'https://www.googleapis.com/auth/business.manage',
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/userinfo.profile',
            'https://www.googleapis.com/auth/drive'
        ]
    ];

    const DATA_LAYER_CONFIG = [
        "driver"    => "mysql",
        "host"      => "localhost",
        "port"      => "3306",
        "dbname"    => "google_my_business",
        "username"  => "root",
        "passwd"    => "root",
        "options"   => [
            PDO::MYSQL_ATTR_INIT_COMMAND    => "SET NAMES utf8",
            PDO::ATTR_ERRMODE               => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE    => PDO::FETCH_OBJ,
            PDO::ATTR_CASE                  => PDO::CASE_NATURAL
        ]
    ];

    const TABLES = array(
        'logs'      => 'application_logs',
        'accounts'  => 'accounts',
        'locations' => 'locations'
    );

    /**
     * @param string|null $uri
     * @return string
     */
    function url(string $uri = null): string
    {
        if($uri) { return ROOT . $uri; }
        return ROOT;
    }