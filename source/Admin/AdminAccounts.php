<?php

namespace Source\Admin;

use League\Plates\Engine;
use Source\Controller\Admin\LogsController;
use Source\Controller\Admin\AccountsControllerAdmin;

class AdminAccounts
{
    private Engine $view;
    private AccountsControllerAdmin $accounts;

    public function __construct()
    {
        $this->view     = Engine::create(__DIR__."/../../theme/views/AdminArea", "phtml");
        $this->accounts = new AccountsControllerAdmin();
    }

    public function getAccounts() {
        $auth_users = [ "jonas.oliveira@karma-network.com", "alexadre.marreiros@karma-network.com" ];

        if(in_array($_SESSION['client_info']['email'], $auth_users)) {

            $accounts = $this->accounts->getAccounts();
            $list = array();

            foreach ($accounts as $account) {  array_push($list, $account->data);  }

            $render_data = array(
                "page"      => "Admin area",
                "title"     => "Home",
                "user"      => $_SESSION['client_info']['name'],
                "accounts"  => $list
            );
    
            echo $this->view->render("accounts", $render_data);
        } else {
            echo $this->view->render("userNotAuth");
        }
    }
}