<?php

namespace Source\Admin;

use League\Plates\Engine;
use Source\Controller\SystemConfigController;

class AdminConfigs
{
    private Engine $view;
    private SystemConfigController $systemConfigController;

    public function __construct()
    {
        $this->view     = Engine::create(__DIR__."/../../theme/views/AdminArea", "phtml");
        $this->systemConfigController = new SystemConfigController;
    }

    public function getConfigs() {
        $configs = $this->systemConfigController->listConfigs();
        $configs = json_decode($configs->data->configs);

        $render_data = array(
            "page"      => "Admin area | Settings",
            "title"     => "Settings",
            "configs"   => $configs
        );

        echo $this->view->render("configs", $render_data);
    }
}