<?php

namespace Source\Admin;

use League\Plates\Engine;
use Source\Controller\Admin\LogsController;
use Source\Controller\Admin\LocationsControllerAdmin;
use Source\Controller\Admin\AccountsControllerAdmin;

class AdminArea
{
    private Engine $view;
    private LogsController $logs;
    private LocationsControllerAdmin $locations;
    private AccountsControllerAdmin $accounts;

    public function __construct($router)
    {
        //View representation
        $this->view     = Engine::create(__DIR__."/../../theme/views/AdminArea", "phtml");
        $this->logs     = new LogsController();
        $this->locations= new LocationsControllerAdmin();
        $this->accounts = new AccountsControllerAdmin();

        $this->view->addData(['router' => $router]);
    }

    public function home() {
        if($_SESSION['client_info']['email'] == "jonas.oliveira@karma-network.com" OR $_SESSION['client_info']['email'] == "alexadre.marreiros@karma-network.com") {
            $render_data = array(
                "page"  => "Admin area",
                "title" => "Home",
                "user"  => $_SESSION['client_info']['name']
            );
    
            echo $this->view->render("home", $render_data);
        } else {
            echo $this->view->render("userNotAuth");
        }
    }

    public function getLogs() {
        $auth_users = [ "jonas.oliveira@karma-network.com", "alexadre.marreiros@karma-network.com" ];

        if(in_array($_SESSION['client_info']['email'], $auth_users)) {
            $info = json_decode(file_get_contents('php://input'));
            $page = isset($info->page) ? $info->page : 1;

            $this->logs->itensLimit = $info->itensPerPage;
            $this->logs->logType    = $info->logType;
            $this->logs->orderBy    = $info->orderBy;
            $this->logs->order      = $info->order;
            $this->logs->dateInit   = $info->startDate;
            $this->logs->dateFinal  = $info->finishDate;


            if(isset($info->page)) { 
                $this->logs->offset = intval($this->logs->itensLimit) * $info->page;
            }
            
            $all_logs = $this->logs->getLogs();
            $pages = $all_logs['num_logs'] / $this->logs->itensLimit;
        
            if($all_logs['num_logs'] == 0) {
                echo json_encode(['logs' => 'No posts found!!!', 'pages' => ceil($pages), 'current_page' => $page]);
            } else {
                $log_list = [];
                foreach ($all_logs['logs'] as $log) { array_push($log_list, $log->data); }
                echo json_encode(['logs' => $log_list, 'pages' => ceil($pages), 'current_page' => $page]);
            }
        }
    }

    public function createCsv() {
        $auth_users = [ "jonas.oliveira@karma-network.com", "alexadre.marreiros@karma-network.com" ];

        if(in_array($_SESSION['client_info']['email'], $auth_users)) {
            $info = json_decode(file_get_contents('php://input'));

            $this->logs->itensLimit = null;
            $this->logs->logType    = $info->logType;
            $this->logs->orderBy    = $info->orderBy;
            $this->logs->order      = $info->order;
            $this->logs->dateInit   = $info->startDate;
            $this->logs->dateFinal  = $info->finishDate;
            
            $all_logs = $this->logs->getLogs();
            
            $file = fopen( 'php://output', 'w' );
            $header = array('date', 'user', 'type', 'action', 'action_details');
            fputcsv($file, $header, ";");

            foreach ($all_logs['logs'] as $log) { 
                $__LOG = $log->data;

                $data = array($__LOG->date, $__LOG->user, $__LOG->type_action, $__LOG->action, $__LOG->action_details);
                fputcsv($file, $data, ";");
            }

            // close the file
            fclose($file);
    
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=\"kgmb.csv\"");
            header("Expires: 0");
            flush();
        }
    }
}