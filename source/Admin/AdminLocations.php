<?php

namespace Source\Admin;

use League\Plates\Engine;
use Source\Controller\Admin\LocationsControllerAdmin;

class AdminLocations
{
    private Engine $view;
    private LocationsControllerAdmin $accounts;

    public function __construct()
    {
        $this->view     = Engine::create(__DIR__."/../../theme/views/AdminArea", "phtml");
        $this->locations= new LocationsControllerAdmin();
    }

    public function getLocations() {
        $auth_users = [ "jonas.oliveira@karma-network.com", "alexadre.marreiros@karma-network.com" ];

        if(in_array($_SESSION['client_info']['email'], $auth_users)) {

            $locations  = $this->locations->getLocations();
            $list       = array();

            foreach ($locations as $location) {  array_push($list, $location->data);  }

            $render_data = array(
                "page"      => "Admin area",
                "title"     => "Home",
                "user"      => $_SESSION['client_info']['name'],
                "locations" => $list
            );
    
            echo $this->view->render("locations", $render_data);
        } else {
            echo $this->view->render("userNotAuth");
        }
    }
}