<?php


namespace Source\App;
use League\Plates\Engine;
use Source\Controller\LocalPostController;
use Source\Controller\LocationsController;
use Source\Controller\AccountsController;
use Source\Controller\LogController;

class WebLocalPost
{
    //List all local post from location
    private LocalPostController $localPost;
    private LocationsController $locations;
    private AccountsController $accountController;
    private Engine $view;

    public function __construct()
    {
        $this->localPost = new LocalPostController();
        $this->locations = new LocationsController;
        $this->logsController = new LogController();
        $this->accountController = new AccountsController();

        $this->view = Engine::create(__DIR__."/../../theme/views/LocalPosts/", "phtml");
    }

    public function getLocalPost($data) {

        if(isset($data['localPostId']) != "") {
            $listPost = $this->localPost->getLocalPostLocation($data['account'], $data['location'], $data['localPostId']);
        } else {
            $listPost = $this->localPost->getLocalPostLocation($data['account'], $data['location']);
        }

        $location = $this->locations->getLocationInfo($data['account'], $data['location']);

        $render_data = array(
            "page"          => "Local Post List",
            "title"         => "Posts List | ". SITE,
            "posts_list"    => json_decode($listPost),
            "location"      => $data['location'],
            "account"       => $data['account'],
            "location_info" => json_decode($location)
        );

        echo $this->view->render("listLocalPosts", $render_data);
    }

    public function createLocalPost($data)
    {
        $post_data  = json_decode(file_get_contents('php://input'));
        $account    = $post_data->account;
        $posts      = $post_data->posts;

        $account_details = json_decode($this->accountController->accountInfo($account));

        $log_data = array(
            "user"          => $_SESSION['client_info']['name'],
            "type_action"   => "application_create_post",
            "action"        => "",
            "action_details"=> ""
        );

        if(isset($post_data->locations)) {
            $locations_qtd = 0;
            $posts_qtd = 0;
            foreach ($post_data->locations as $location) {
                $location_details = json_decode($this->locations->getLocationInfo($account, $location));

                foreach ($posts as $post) {
                    $result = json_decode($this->localPost->newCreateLocalPost($post, $account, $location));
                    $posts_qtd++;

                    if(isset($result->error)) {
                        $log_data['log_type'] = 'error';
                        $log_data['action'] = "Create post in account \"{$account_details->accountName}\" and location \"{$location_details->title}\"";
                        $log_data['action_details'] = $result->error->status . ': ' .$result->error->message;
                    } else {
                        $log_data['log_type'] = 'success';
                        $log_data['action'] = "Create post in account \"{$account_details->accountName}\" and location \"{$location_details->title}\"";
                        $log_data['action_details'] = "Post text: \"{$result->summary}\" created";
                    }
                    
                    $this->logsController->registerlog($log_data);
                }
                $locations_qtd++;
            }

            echo "Way created {$posts_qtd} posts in {$locations_qtd} location";
        } 
        
        else {
            $location   = $post_data->location;
            $posts_qtd = 0;

            foreach ($posts as $post) {
                $location_details = json_decode($this->locations->getLocationInfo($account, $location));
                $result = json_decode($this->localPost->newCreateLocalPost($post, $account, $location));
                $posts_qtd++;

                if(isset($result->error)) {
                    $log_data['log_type'] = 'error';
                    $log_data['action'] = "Create post in account \"{$account_details->accountName}\" and location \"{$location_details->title}\"";
                    $log_data['action_details'] = $result->error->status . ': ' .$result->error->message;
                } else {
                    $log_data['log_type'] = 'success';
                    $log_data['action'] = "Create post in account \"{$account_details->accountName}\" and location \"{$location_details->title}\"";
                    $log_data['action_details'] = "Post text: \"{$result->summary}\" created";
                }

                $this->logsController->registerlog($log_data);
            }

            echo "Way created {$posts_qtd} posts in 1 location";
        }
    }

    public function createLocalPostAlert($data)
    {
        $post_data  = json_decode(file_get_contents('php://input'));
        $account    = $post_data->account;
        $location   = $post_data->location;
        $posts      = $post_data->posts;

        $account_details = json_decode($this->accountController->accountInfo($account));
        $location_details = json_decode($this->locations->getLocationInfo($account, $location));

        $log_data = array(
            "user"          => $_SESSION['client_info']['name'],
            "type_action"   => "application_create_alert",
            "action"        => "",
            "action_details"=> ""
        );

        foreach ($posts as $post) {
            $result = json_decode($this->localPost->newCreateLocalPost($post, $account, $location));

            $log_data['action'] = "Create alert in account \"{$account_details->accountName}\" and location \"{$location_details->title}\"";
            $log_data['action_details'] = "Post text: \"{$result->summary}\" created";
            $this->logsController->registerlog($log_data);
        }
        echo json_encode($post_data);
    }

    public function createLocalPostNew($data)
    {
        $post_data  = json_decode(file_get_contents('php://input'));
        $account    = $data['account'];
        $location   = $data['location'];
        $localPostData = $post_data;

        echo json_encode($this->localPost->newCreateLocalPost($localPostData, $account, $location));
    }

    public function createMultiplesPost($data)
    {

        /*$file = fopen(__DIR__ . "/../../Posts farmácias portuguesas.json", "r");
        $file = fread($file, filesize("Posts farmácias portuguesas.json"));*/

        $info           = json_decode(file_get_contents('php://input'));
        $account        = $data['account'];
        $allLocations   = $info->locations;
        $allPosts       = $info->posts;

        $iterator = 1;
        $output = array();

        try {

            foreach ($allLocations as $location) {

                foreach ($allPosts as $post) {
                    $post_created = $this->localPost->createLocalPost($post, $account, $location->id);
                    array_push($output, array('location' => $location->id, 'post' => json_decode($post_created) ));
                }

                $iterator++;
                if($iterator % 100 == 0) {
                    $output['iterator'] = $iterator;
                }
            }
        } catch (\Exception $e) {
            $info->error = $e->getMessage();
        }

        echo json_encode($output);
    }

    public function deleteLocalPost($data)
    {
        $info = json_decode(file_get_contents('php://input'));
        $account  = $info->account;
        
        $account_details = json_decode($this->accountController->accountInfo($account));

        $log_data = array(
            "user"          => $_SESSION['client_info']['name'],
            "log_type"      => 'success',
            "type_action"   => "application_remove_post",
            "action"        => "",
            "action_details"=> ""
        );

        if(isset($info->list)) {
            $posts_qtd      = 0;
            $locations_qtd  = 0;

            if(isset($info->list->posts) ) {
                foreach ($info->list as $key => $item) {
                    $locations_qtd++;
                    $location_details = json_decode($this->locations->getLocationInfo($account, $item->location));

                    foreach ($item->posts as $post) {
                        $this->localPost->deleteLocalPost($account, $item->location, $post);
                        $posts_qtd++;

                        $log_data['action'] = "Remove post in account \"{$account_details->accountName}\" and in location \"{$location_details->title}\"";
                        $log_data['action_details'] = "Post text: \"{$post->summary}\" removed";
                        $this->logsController->registerlog($log_data);
                    }
                }


            } else {
                foreach ($info->list->locations as $key => $location) {
                    $posts = $this->localPost->getLocalPostLocation($account, $location);
                    $posts = json_decode($posts);
                    $locations_qtd++;
                    $location_details = json_decode($this->locations->getLocationInfo($account, $location));

                    foreach ($posts->localPosts as $post) {
                        $postInfo = explode('/', $post->name);
                        $posts_qtd++;
                        $this->localPost->deleteLocalPost($account, $location, $postInfo[5]);

                        $log_data['action'] = "Remove post in account \"{$account_details->accountName}\" and in location \"{$location_details->title}\"";
                        $log_data['action_details'] = "Post text: \"{$post->summary}\" removed";
                        $this->logsController->registerlog($log_data);
                    }
                }
            }

        } else {
            $location = $info->location;
            $posts    = $info->posts;
            $posts_qtd= 0;

            $location_details = json_decode($this->locations->getLocationInfo($account, $location));
    
            foreach ($posts as $key => $post) {
                $this->localPost->deleteLocalPost($account, $location, $post->post_id);
                $posts_qtd++;

                $log_data['action'] = "Remove post in account \"{$account_details->accountName}\" and in location \"{$location_details->title}\"";
                $log_data['action_details'] = "Post text: \"{$post->summary}\" removed";
                $this->logsController->registerlog($log_data);
            }
        }
        
        echo "The post was removed successful";
    }

    public function deleteMultiplesLocalPost($data)
    {

        $posts_list  = json_decode(file_get_contents('php://input'));

        foreach ($posts_list as $item) {
            $post_to_delete = array(
                'account'       => $data['account'],
                'location'      => $data['location'],
                'localPostId'   => $item
            );

            $this->localPost->deleteLocalPost($data['account'], $data['location'], $item);
        }


        echo "Foram deletados" . sizeof($posts_list)." posts no total...";
    }

    public function getAllLocalPostForAccount($data) {
        $account = $data['account'];

        $account_controller = new AccountsController;
        $account_info = json_decode($account_controller->accountInfo($account));

        $locations = $this->locations->listAllLocations($account);
        $result = json_encode($this->localPost->listAllLocalPostForAccount($locations, $account));

        $render_data = array(
            "page"          => "Local Post List",
            "title"         => "Posts List | ". SITE,
            "posts_list"    => json_decode($result),
            "account"       => $data['account'],
            "account_info"  => $account_info
        );

        echo $this->view->render("listLocalPostsAccount", $render_data);
    }

    public function cloneAllLocalPostForAccount() {
        $data       = json_decode(file_get_contents('php://input'));
        $account    = $data->account;
        $post_list  = ['localPosts' => [] ];

        if(!$account) {
            echo json_encode(["code" => 500, "msg" => "The account id was't informed!"]);
            return;
        }

        /*$locations = json_decode($this->locations->listAllLocations([$account]));
        $localPost_list = $this->localPost->listAllLocalPOstForAccount($locations->locations);

        foreach ($localPost_list as $key => $item) {
            $localPost_items = $item['local_post'];
            if(is_array($localPost_items) OR is_object($localPost_items)) {
                foreach ($localPost_items->localPosts as $post) {
                    array_push($post_list['localPosts'], $post);
                }
            }
        }

        $post_list = json_decode(json_encode($post_list), FALSE);

        $result = $this->localPost->repeatPost($account, null, null, $post_list);

        echo json_encode($result);*/
    }

    public function cloneAllLocalPostForLocation() {
        $data       = json_decode(file_get_contents('php://input'));
        $account    = $data->account;
        $location   = $data->location;

        if(!$account) {
            echo json_encode(["code" => 500, "msg" => "The account id was't informed!"]);
            return;
        }
        if(!$location) {
            echo json_encode(["code" => 500, "msg" => "The account id was't informed!"]);
            return;
        }

        $localPost_list = json_decode($this->localPost->getLocalPostLocation($account, $location));
        $result = $this->localPost->repeatPost($account, $location, null, $localPost_list);

        echo json_encode($result);
    }

    public function clonePost()
    {
        $data           = json_decode(file_get_contents('php://input'));
        $account_id     = $data->account;
        $location_id    = $data->location;
        $local_post_id  = $data->localPostId;

        $post = $this->localPost->getLocalPostLocation($account_id, $location_id, $local_post_id);

        $output = null;
        if($account_id && $location_id && $local_post_id) {
            $post = json_decode($post);
            $output = json_encode($this->localPost->repeatPost($account_id, $location_id, $post)[0]);
        }

        else {
            if(!$account_id) {
                $output = json_encode(["code" => 500, "msg" => "The account id not was informed!"]);
            }
            if(!$location_id) {
                $output = json_encode(["code" => 500, "msg" => "The location id not was informed!"]);
            }
            if(!$local_post_id) {
                $output = json_encode(["code" => 500, "msg" => "The post id not was informed!"]);
            }
        }

        echo $output;
    }

    public function clonePostV2()
    {
        $data   = json_decode(file_get_contents('php://input'));
        $output = ['code' => '200', 'msg' => 'Post has cloned successful!'];

        foreach ($data as $key => $item) {
            $account = $item->account;
            $location= $item->location;

            foreach ($item->posts as $key => $post) {
                $post_id = $post->post_id;
                $post_info = $post->post_detail;

                $clone_status = $this->localPost->repeatPost($account, $location, $post_info);

                if($clone_status[0]['code'] != '200') {
                    $output['code']= $clone_status[0]['code'];
                    $output['msg'] = $clone_status[0]['msg'];
                } else {
                    $output['msg'] = $clone_status[0]['msg'];
                    //Remove the post
                    $this->localPost->deleteLocalPost($account, $location, $post_id);
                }
            }
        }
        
        echo json_encode($output);
    }

    public function getMetrics($data)
    {        
        echo json_encode( $this->localPost->listMetrics($data['account'], $data['location'], $data['localPostId']) );
    }

    public function relaunchPostsAccountForLocations($data) {
        $info  = json_decode(file_get_contents('php://input'));
        $listPost = array();

        foreach ($info->locations as $location) {
            $posts = json_decode($this->localPost->getLocalPostLocation($info->account, $location));

            foreach ($posts->localPosts as $post) { 

                $data = array(
                    'languageCode'  => $post->languageCode,
                    'summary'       => $post->summary,
                    'topicType'     => $post->topicType,
        
                    'callToAction'  => array(
                        'actionType'=> $post->callToAction->actionType,
                        'url'       => $post->callToAction->url
                    )
                );

                if($post->topicType != 'ALERT') {
                    $images = array();
                    
                    foreach ($post->media as $media) {
                        array_push($images, ["mediaFormat" => $media->mediaFormat, "sourceUrl" => $media->googleUrl]);
                    }

                    $data['media']      = $images;
                } else {
                    $data['alertType']  = $post->alertType;
                }


                $post_details = explode('/', $post->name);
                $this->localPost->newCreateLocalPost($data, $info->account, $location);
                $this->localPost->deleteLocalPost($info->account, $location, $post_details[5]);            
                array_push($listPost, $post); 
            }
        }

        echo json_encode($listPost);
    }
}