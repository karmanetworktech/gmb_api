<?php


namespace Source\App;

use CoffeeCode\DataLayer\Connect;
use League\Plates\Engine;
use Source\Controller\LocationsController;
use Source\Controller\AccountsController;
use Source\Controller\LogController;

class Location
{
    /** @var Engine @var Engine */
    private Engine $view;

    private LocationsController $locationsController;
    private AccountsController $accountController;
    private LogController $logController;

    public function __construct($router)
    {
        //Controllers list
        $this->locationsController  = new LocationsController;
        $this->logController        = new LogController;
        $this->accountController    = new AccountsController;

        //View representation
        $this->view = Engine::create(__DIR__."/../../theme/views", "php");
        $this->view->addData(["router" => $router]);
    }

    public function accountLocations($data) {
        $account_info   = $this->accountController->accountInfo($data['account']);

        $render_data = array(
            "page"              => "Locations List",
            "title"             => "Location List | ". SITE,
            "account"           => $data['account'],
            "account_info"      => json_decode($account_info)
        );

        echo $this->view->render("listLocations", $render_data);
    }

    public function getAllLocations($data)
    {

        if(isset($_GET['next_page'])) {
            $locations_list = $this->locationsController->listAllLocations($data['account'], $_GET['next_page'], 12);
        } else {
            $locations_list = $this->locationsController->listAllLocations($data['account'], null, 12);
        }
        
        $account_info   = $this->accountController->accountInfo($data['account']);
        
        //Register location in database
        if($locations_list['locations']) {
            foreach ($locations_list['locations'] as $location) {
                $loc = explode('/', $location->name);
                $this->locationsController->registerLocation($data['account'], $loc[1]);
            }
        }


        $next_page = strval( intval($_GET['page']) + 1 );
        if(isset($locations_list['next_page'])) {
            if(!isset($_SESSION['locations_paginate_tokens'][$next_page])) {
                $_SESSION['locations_paginate_tokens'][$next_page] = $locations_list['next_page'];
            } 
        }

        //unset($_SESSION['locations_paginate_tokens']);

        $render_data = array(
            "page"              => "Locations List",
            "title"             => "Location List | ". SITE,
            "locations_list"    => $locations_list['locations'],
            "next_page"         => $locations_list['next_page'],
            "account"           => $data['account'],
            "account_info"      => json_decode($account_info),
            'next_page_num'     => $next_page,
            'prev_page_num'     => intval($_GET['page']) - 1
        );

        echo $this->view->render("fragment/locationList", $render_data);
    }
}

