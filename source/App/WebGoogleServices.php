<?php


namespace Source\App;
use Source\Controller\GoogleDriveController;
use Source\Controller\GoogleSheetsController;
use Source\Controller\LocationsController;
use function GuzzleHttp\Promise\all;

class WebGoogleServices
{
    public function listFiles()
    {
        try {
            $google_drive = new GoogleDriveController();
            echo json_encode($google_drive->listAllFiles());
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getSheetValues($data)
    {
        $output = null;
        if($data['id']) {
            $google_sheet = new GoogleSheetsController($data['id']);

            $output = $google_sheet->getValues();
        }

        echo $output;
    }

    public function showSheetValues($data)
    {
        $body_request = json_decode(file_get_contents('php://input'));

        $sheet_id   = $data['sheet_id'];
        $account    = $data['account'];
        $location   = isset($data['location']) != "" ? $data['location'] : NULL;
        $sheet_value= array();
        $value_list = null;
        $locations  = new LocationsController();
        $filters    = $body_request->filter_date;

        $index_list = array(
            'summary'       => $body_request->post_text,
            'img_url'       => $body_request->link_img,
            'action_url'    => $body_request->action_link,
            'filter_date'   => $body_request->date_column
        );

        //Get sheet values
        $google_sheet = new GoogleSheetsController($data['sheet_id']);
        if($sheet_id) {
            $sheet_value = json_decode($google_sheet->getValues());
        }

        if(sizeof($sheet_value->values) < 2 ) {
            echo ["code" => 500, "msg" => "No posts find in this sheet, please set post in your sheet!"];
            die();
        }

        //To format the values to object
        if($sheet_value and sizeof($sheet_value->values) > 1) {
            $value_list = $google_sheet->formatValues($sheet_value->values, $index_list, $filters);
        }

        if($location == null) {
            $locations_open = array();
            $all_locations = $locations->listAllLocations($account);

            if (sizeof($all_locations['locations']) > 0) {
                foreach ($all_locations['locations'] as $location) {
                    if ($location->openInfo->status == 'OPEN' && !isset($location->metadata->hasGoogleUpdated) !== false) {
                        array_push($locations_open, $location);
                    }
                }
            }

            $output = array('locations' => $locations_open, 'posts' => $value_list);
        } else {
            $output = array('locations' => $location, 'posts' => $value_list);
        }

        //Return list of post to create
        echo json_encode($output);
    }
}