<?php


namespace Source\App;

use CoffeeCode\DataLayer\Connect;
use League\Plates\Engine;
use Source\Controller\GoogleLoginController;
use Source\Controller\LocalPostController;
use Source\Controller\LocationsController;
use Source\Controller\AccountsController;
use Source\Controller\LogController;
use Source\Controller\SystemConfigController;
use Source\Helpers\Mail;
use Source\Helpers\TokenIsValid;

class Web
{
    private Engine $view;
    private GoogleLoginController $google;
    private LocalPostController $localPostsController;
    private LocationsController $locationsController;
    private AccountsController $accountController;
    private SystemConfigController $systemConfigController;
    private LogController $logController;

    public function __construct()
    {
        $this->google = new GoogleLoginController();

        //Controllers list
        $this->localPostsController     = new LocalPostController();
        $this->locationsController      = new LocationsController;
        $this->accountController        = new AccountsController;
        $this->logsController           = new LogController;
        $this->systemConfigController   = new SystemConfigController;

        //View representation
        $this->view = Engine::create(__DIR__."/../../theme/views", "php");
    }

    public function home()
    {
        $token_valid = new TokenIsValid;
        $url_origin  = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] . '';
        if(!$token_valid->testTokenValidate() && $url_origin != ROOT.'/' && strpos($url_origin, 'code') === false) {
            session_unset();
            header("Location: " . ROOT);
            die();
        }

        if(!$token_valid->testTokenValidate()){
            unset($_SESSION['time_to_refresh'], $_SESSION['client_info'], $_SESSION['google_token']);
        }

        $render_data = array(
            "page"      => "Home page",
            "title"     => "Home | ". SITE,
            "login_url" => $this->google->getAuthorizationUrl(),
            "configs"   => $this->systemConfigController->listConfigs()->data->configs
        );

        echo $this->view->render("home", $render_data);
    }

    public function login()
    {
        if(empty($_SESSION['google_token'])) {
            $error = filter_input(INPUT_GET, 'error', FILTER_SANITIZE_STRIPPED);
            $code  = filter_input(INPUT_GET, 'code', FILTER_SANITIZE_STRIPPED);

            if($error) { echo "<h3>You need to authorize to continue!</h3>"; }

            if($code) {
                $_SESSION['google_token'] = serialize($this->google->getToken($code));
                $url = GOOGLE['redirectUri'];

                //Register log
                $log_data = array(
                    "user"              => $_SESSION['client_info']['name'],
                    "log_type"          => 'success',
                    "type_action"       => "application_login",
                    "action"            => "Succesful login in application",
                    "action_details"    => "The user \"{$_SESSION['client_info']['name']}\" has logged in app"
                );
                $this->logsController->registerlog($log_data);

                header("Location: " . $url);
                die;
            }
            header("Location: " . ROOT);
            die;
        }

        else {
            $off = filter_input(INPUT_GET, 'off', FILTER_VALIDATE_BOOLEAN);
            if($off) { $this->google->logout(); }

            header("Location: " . ROOT);
            die;
        }
    }

    public function getAccounts() {
        if(isset($_SESSION['locations_paginate_tokens'])) {
            unset($_SESSION['locations_paginate_tokens']);
        }

        $accounts = new AccountsController;
        $accounts = $accounts->listAllAccounts();

        $text = "Accounts list";
        echo $this->view->render("listAccounts", [
            "page"          => $text,
            "title"         => "List Accounts | ". SITE,
            "list_account"  => $accounts,
            "image_default" => __DIR__ .'/../../theme/assets/img/store.png'
        ]);
    }

    public function error(array $data)
    {
        echo "<h1>Error {$data["errcode"]}</h1>";
    }

    public function cronRepeaterPost($data)
    {
        if(empty($_SESSION['google_token'])) {
            $code  = filter_input(INPUT_GET, 'code', FILTER_SANITIZE_STRIPPED);
            if($code) {
                $_SESSION['google_token'] = serialize( $this->google->getToken($code) );
                header("Location: ".GOOGLE['redirectUri']."/cron");
                die;
            }

            header("Location: {$this->google->getAuthorizationUrl()}");
        }

        else {
            $this->localPostsController->repeatPost($data['account'], $data['location'], null);
        }
    }

    public function getAllLocations($data)
    {
        if(isset($_GET['next_page'])) {
            $locations_list = $this->locationsController->listAllLocations($data['account'], $_GET['next_page'], 12);
        } else {
            $locations_list = $this->locationsController->listAllLocations($data['account'], null, 12);
        }
        
        $account_info   = $this->accountController->accountInfo($data['account']);
        
        //Register location in database
        if($locations_list['locations']) {
            foreach ($locations_list['locations'] as $location) {
                $loc = explode('/', $location->name);
                $this->locationsController->registerLocation($data['account'], $loc[1]);
            }
        }

        $render_data = array(
            "page"              => "Locations List",
            "title"             => "Location List | ". SITE,
            "locations_list"    => $locations_list['locations'],
            "next_page"         => $locations_list['next_page'],
            "account"           => $data['account'],
            "account_info"      => json_decode($account_info)
        );

        echo $this->view->render("listLocations", $render_data);
    }

    public function getLocationsList($data)
    {
        $locations = array();

        $locations_list = $this->locationsController->listAllLocations($data['account']);
        foreach ($locations_list['locations'] as $location) {
            array_push($locations, $location);
        }

        while( $locations_list['next_page'] ){
            $locations_list = $this->locationsController->listAllLocations($data['account'], $locations_list['next_page']);
            
            foreach ($locations_list['locations'] as $location) {
                array_push($locations, $location);
            }
        };

        echo json_encode($locations);
    }

    public function getLocaleService($data)
    {
        $this->view = Engine::create(__DIR__."/../../theme/views/LocationsInfo/", "phtml");

        $account  = $data['account'];
        $location = $data['location'];

        $output = $this->locationsController->getServicesLocation($account, $location);

        $render_data = array(
            "page"          => "Lista de serviços",
            "title"         => "Posts List | ". SITE,
            "location"      => $location,
            "account"       => $account,
            "location_serv" => json_decode($output)
        );

        echo $this->view->render("services", $render_data);
    }

    public function test()
    {
        $conn = Connect::getInstance();
        $error = Connect::getError();

        if($error) {
            echo $error->getMessage();
            die();
        }

        var_dump(true);
    }

    public function sendMail()
    {
        $email_info = json_decode(file_get_contents('php://input'));
        $mail = new Mail();

        $mail->add(
            $email_info->title,
            $email_info->body,
            $_SESSION['client_info']['name'],
            $_SESSION['client_info']['email']
        )->send();

        if($mail->error()) {
            echo $mail->error()->getMessage();
        } else {
            echo "Email sending successful";
        }
    }

    public function applicationLogin() {
        $this->view = Engine::create(__DIR__."/../../theme/views/", "phtml");

        $render_data = array(
            "page"          => "Application login",
            "title"         => "App login | ". SITE,
        );

        echo $this->view->render("appLogin", $render_data);
    }

    public function applicationLoginRegister($data) {
        if($data['pass'] == '2ThDzTE%*2GgE4_#xyb@') {
            $_SESSION['app_logged'] = true;
            echo json_encode(array('status' => 200));
        } else {
            echo json_encode(array('status' => 400));
        }
    }
}

