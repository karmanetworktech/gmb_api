<?php


namespace Source\App;
use Source\Controller\ReviewsController;

class WebReviews
{
    private ReviewsController $reviews_controller;

    public function __construct()
    {
        $this->reviews_controller = new ReviewsController();
    }

    /**
     * @param $data
     */
    public function getReviews($data)
    {
        $account    = $data['account'];
        $location   = $data['location'];
        echo json_encode($this->reviews_controller->listReviews($account, $location));
    }

}