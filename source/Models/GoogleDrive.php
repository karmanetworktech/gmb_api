<?php


namespace Source\Models;
use Source\Helpers\Curl;

class GoogleDrive
{
    public function fileList()
    {
        $filter = "q=mimeType='application/vnd.google-apps.spreadsheet'";
        $url = DRIVE_DEFAULT_LINK . "/files?". $filter;
        $curl = new Curl($url, "GET");

        return $curl->executeCurl();
    }
}