<?php


namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class SystemConfig extends DataLayer
{
    public function __construct()
    {
        parent::__construct('system_config', ['configs'], 'id', false);
    }

    public function getConfigs() {
        return $this->find()->fetch();
    }

    public function setConfigs($data) {
        $config = new SystemConfig();

        $config->configs   = $data;
        $config->save();

        return $config;
    }
}