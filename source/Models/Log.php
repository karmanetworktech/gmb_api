<?php


namespace Source\Models;

use CoffeeCode\DataLayer\DataLayer;

class Log extends DataLayer
{
    public function __construct()
    {
        parent::__construct(TABLES['logs'], ['user', 'log_type', 'action', 'type_action', 'action_details'], 'id', false);
    }

    public function addLog($data): Log
    {
        $this->id = null;
        
        $this->user             = $data['user'];
        $this->action           = $data['action'];
        $this->log_type         = $data['log_type'];
        $this->type_action      = $data['type_action'];
        $this->action_details   = $data['action_details'];
        $this->save();

        return $this;
    }
}