<?php


namespace Source\Models;
use Source\Helpers\Curl;
use Source\Helpers\TokenIsValid;

class LocalPosts
{
    public $accountId;
    public $locationId;
    public $localPostId;
    private $tokenIsValid;

    /**
     * Accounts constructor.
     * @param $accountId
     * @param $locationId
     * @param $localPostId
     */
    public function __construct($accountId = null, $locationId = null, $localPostId = null)
    {
        $test = new TokenIsValid;
        $this->accountId    = $accountId;
        $this->locationId   = $locationId;
        $this->localPostId  = $localPostId;

        $this->tokenIsValid = $test->testTokenValidate();
    }

    public function getLocalPostFromLocation()
    {
        if($this->tokenIsValid) {
            $urlBase = GMB_DEFAULT_LINK . "/accounts/{$this->accountId}/locations/{$this->locationId}/localPosts";
            $url = $this->localPostId = "" ? $urlBase : $urlBase . "/{$this->localPostId}";
            $curl = new Curl($url.'?pageSize=100', "GET");

            return $curl->executeCurl();
        } else {
            $url = GOOGLE['redirectUri'].'/login';
            header("Location: {$url}");
        }

    }

    public function createLocalPost($data)
    {
        $data = !is_object($data) ? json_decode(json_encode($data)) : $data;
        $localPost = array();

        /*
         * Local post standard
         * necessary(summary, languageCode, media[mediaFormat:photo, sourceUrl])
         * optional(topicType: standard, callToAction[actionType: LEARN_MORE, url: url destiny])
         */
        $localPost["languageCode"]  = $data->languageCode;
        $localPost["summary"]       = $data->summary;

        if(isset($data->media)) {
            $media = !is_object($data->media) ? json_decode(json_encode($data->media)) : $data->media;
            $localPost["media"]         = array(
                "mediaFormat"   => isset($media->mediaFormat) ? $media->mediaFormat : $media[0]->mediaFormat,
                "sourceUrl"     => isset($media->sourceUrl) ? $media->sourceUrl : $media[0]->sourceUrl
            );
        }

        if(isset($data->callToAction->url) != "") {
            $localPost["callToAction"]['actionType'] = $data->callToAction->actionType;
            $localPost["callToAction"]['url'] = $data->callToAction->url;
        }

        if(strtoupper($data->topicType) == "STANDARD") {
            $localPost["topicType"] = strtoupper($data->topicType);
        } else {
            $localPost["topicType"] = strtoupper($data->topicType);
            if($localPost["topicType"] = strtoupper($data->topicType) == "EVENT"){
                $event = !is_object($data->event) ? json_decode(json_encode($data->event)) : $data->event;
                $localPost['event']['title']    = $event[0]->title;
                $localPost['event']['startDate']= $event[0]->startDate;
                $localPost['event']['endDate']  = $event[0]->endDate;
            }
        }

        $url = GMB_DEFAULT_LINK . "/accounts/{$this->accountId}/locations/{$this->locationId}/localPosts";
        $curl = new Curl($url, 'POST');

        return $curl->executeCurl( json_encode($localPost) );
    }

    public function newCreateLocalPost($data)
    {
        $data = !is_object($data) ? json_decode(json_encode($data)) : $data;
        
        $url = GMB_DEFAULT_LINK . "/accounts/{$this->accountId}/locations/{$this->locationId}/localPosts";
        $curl = new Curl($url, 'POST');

        return $curl->executeCurl( json_encode($data) );
    }

    public function removeLocalPost()
    {
        $url = GMB_DEFAULT_LINK . "/accounts/{$this->accountId}/locations/{$this->locationId}/localPosts/{$this->localPostId}";
        $curl = new Curl($url, 'DELETE');

        return $curl->executeCurl();
    }

    public function listMetrics($created_date)
    {

        $body = array(
            "localPostNames"=> array("accounts/{$this->accountId}/locations/{$this->locationId}/localPosts/{$this->localPostId}"),
            "basicRequest"  => array( 
                "metricRequests" => array (
                    array("metric" => "ALL" )
                )
            ) 
        );

        //$url = GMB_DEFAULT_LINK . "/accounts/{$this->accountId}/locations/{$this->locationId}/localPosts:reportInsights";
        //$curl = new Curl($url, 'POST');

        return json_encode($body);
    }
}