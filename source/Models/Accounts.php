<?php

namespace Source\Models;

use Source\Helpers\Curl;
use CoffeeCode\DataLayer\DataLayer;
class Accounts extends DataLayer
{
    public $accountId;
    private $locationId;

    /**
     * Accounts constructor.
     * @param $accountId
     * @param $locationId
     */

    public function __construct($accountId = null, $locationId = null)
    {
        $this->accountId    = $accountId;
        $this->locationId   = $locationId;

        parent::__construct('accounts', ['account_id',  'complete_data'], 'id', true);
    }

    public function listAllAccounts()
    {
        /** @var $curl */
        $curl = new Curl(GMB_ACCOUNTS_LINK . "/accounts", "GET");

        return $curl->executeCurl();
    }

    public function listAllLocations()
    {
        /** @var $curl */
        $curl = new Curl(GMB_ACCOUNTS_LINK . "/accounts/{$this->accountId}/locations", "GET");

        return $curl->executeCurl();
    }

    public function accountInfo()
    {
        /** @var $curl */
        $curl = new Curl(GMB_DEFAULT_LINK . "/accounts/{$this->accountId}", "GET");

        return $curl->executeCurl();
    }

    public function getAccountInfo() {
        $accountData = $this->find("account_id = :account_id", "account_id={$this->accountId}")->fetch();

        if(!isset($accountData->data)) { 
            $data = $this->accountInfo(); 
            $accountData = $this->registerAccount($data);
        }

        return $accountData->data->complete_data;

    }

    public function registerAccount($data)
    {
        $account = new Accounts();

        $account->id            = null;
        $account->account_id    = $this->accountId;
        $account->complete_data = $data;
        $account->save();

        return $account;
    }
}