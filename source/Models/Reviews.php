<?php


namespace Source\Models;
use Source\Helpers\Curl;

class Reviews
{
    public $account_id;
    public $location_id;

    /**
     * Reviews constructor.
     * @param $account_id
     * @param $location_id
     */
    public function __construct($account_id = null, $location_id = null)
    {
        $this->account_id = $account_id;
        $this->location_id = $location_id;
    }

    public function getReviews()
    {
        /** @var $curl */
        $curl = new Curl(GMB_DEFAULT_LINK . "/accounts/{$this->account_id}/locations/{$this->location_id}/reviews", "GET");

        return $curl->executeCurl();
    }

}