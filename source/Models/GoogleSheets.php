<?php


namespace Source\Models;
use Source\Helpers\Curl;

class GoogleSheets
{
    public $id;

    /**
     * GoogleSheets constructor.
     * @param $id
     */
    public function __construct($id = null)
    {
        $this->id = $id;
    }


    public function getValues()
    {
        $url = SHEETS_DEFAULT_LINK . "/spreadsheets/{$this->id}/values/A1:Z2000";
        $curl = new Curl($url, "GET");

        return $curl->executeCurl();
    }
}