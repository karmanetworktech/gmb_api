<?php

namespace Source\Models\Admin;

use CoffeeCode\DataLayer\DataLayer;

class Accounts extends DataLayer
{
    public function __construct()
    {
        parent::__construct(TABLES['accounts'], ['account_id', 'complete_data'], 'id', true);
    }

    public function loadAccounts() {
        $model = new Accounts();
        return $model->find()->fetch(true);
    }
}