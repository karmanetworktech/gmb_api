<?php

namespace Source\Models\Admin;

use CoffeeCode\DataLayer\DataLayer;

//"type_action = :type_action", "type_action='application_remove_post"

class Logs extends DataLayer
{
    public function __construct()
    {
        parent::__construct(TABLES['logs'], ['user', 'action', 'type_action'], 'id', true);
    }

    public function LoadLogs($itensPerPage, $params_columns, $params_rows, $orderBy, $order, $offset = null) {

        
        $model = new Logs();
        
        $model = $params_columns != '' ? $model->find($params_columns, $params_rows) : $model->find();
        if($itensPerPage) { $model->limit($itensPerPage); }
        if($offset) { $model->offset($offset); }
        
        $logs = $model->order("{$orderBy} {$order}")->fetch(true);

        $num_logs = $model->count();
        

        return array('logs' => $logs, 'num_logs' => $num_logs);
    }
}