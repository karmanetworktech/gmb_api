<?php

namespace Source\Models\Admin;

use CoffeeCode\DataLayer\DataLayer;

class Locations extends DataLayer
{
    public function __construct()
    {
        parent::__construct(TABLES['locations'], ['account_id', 'location_id', 'location_data', 'created_at'], 'id', true);
    }

    public function loadLocations() {
        $model = new Locations();
        return $model->find()->fetch(true);
    }
}