<?php


namespace Source\Models;

use Source\Helpers\Curl;
use CoffeeCode\DataLayer\DataLayer;

class Locations extends DataLayer
{
    public $accountId;
    public $locationId;
    public $page_token;
    public $qtd_per_page;

    /**
     * Accounts constructor.
     * @param $accountId
     * @param $locationId
     * @param $page_token
     */

    public function __construct($accountId = null, $locationId = null, $page_token = null, $qtd_per_page = 100)
    {
        $this->accountId    = $accountId;
        $this->locationId   = $locationId;
        $this->page_token   = $page_token;
        $this->qtd_per_page = 100;

        parent::__construct('locations', ['account_id', 'location_id', 'location_data'], 'id', true);
    }

    public function listAllLocations()
    {
        $fields = "name,phoneNumbers.primaryPhone,storefrontAddress.addressLines,websiteUri,title,openInfo,";
        $fields .= "categories.primaryCategory,metadata";

        /** @var $curl */
        //Locations fields required
        $params = "?readMask={$fields}&pageSize={$this->qtd_per_page}";
        $params = $this->page_token ? $params."&pageToken={$this->page_token}" : $params;
        $url    ="/accounts/{$this->accountId}/locations{$params}";
        $curl   = new Curl(GMB_API_LINKS['locations'] . $url, "GET");

        return $curl->executeCurl();
    }

    public function getLocation()
    {
        $fields = "name,phoneNumbers.primaryPhone,storefrontAddress.addressLines,websiteUri,title,openInfo,";
        $fields .= "categories.primaryCategory,metadata";

        /** @var $curl */
        $params = "?readMask={$fields}";
        $url    = "/locations/{$this->locationId}{$params}";
        $curl   = new Curl(GMB_API_LINKS['locations'] . $url, "GET");

        return $curl->executeCurl();
    }
    
    public function getServices()
    {
        /** @var $curl */
        $url = "/accounts/{$this->accountId}/locations/{$this->locationId}/serviceList";
        $curl = new Curl(GMB_DEFAULT_LINK . $url, "GET");

        return $curl->executeCurl();
    }

    public function locationInfo() {
        $colums = "account_id = :account_id AND location_id = :location_id";
        $values = "account_id={$this->accountId}&location_id={$this->locationId}";
        $locationData = $this->find($colums, $values)->fetch();

        if(!isset($locationData->data)) {
            $data = $this->getLocation();
            $locationData = $this->registerlocation($data);
        }

        return $locationData->data->location_data;
    }

    public function registerlocation($data)
    {
        $location = new Locations();

        $location->account_id   = $this->accountId;
        $location->location_id  = $this->locationId;
        $location->location_data= $data;
        $location->save();

        return $location;
    }
}