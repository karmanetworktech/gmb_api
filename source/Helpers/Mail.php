<?php


namespace Source\Helpers;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use Exception;
use stdClass;

class Mail
{

    /** @var PHPMailer  */
    private $phpmailer;

    /** @var Exception */
    private $error;

    /** @var stdClass */
    private $data;

    public function __construct()
    {
        $this->phpmailer = new PHPMailer(true);
        $this->data = new stdClass();

        $this->phpmailer->isSMTP();
        $this->phpmailer->isHTML();
        $this->phpmailer->setLanguage('br');

        $this->phpmailer->SMTPAuth = true;
        $this->phpmailer->SMTPSecure = 'ssl';
        $this->phpmailer->CharSet = "utf-8";

        $this->phpmailer->Host = CONFIG_SMTP_MAIL['host'];
        $this->phpmailer->Port = CONFIG_SMTP_MAIL['port'];
        $this->phpmailer->Username = CONFIG_SMTP_MAIL['user'];
        $this->phpmailer->Password = CONFIG_SMTP_MAIL['pass'];
    }

    public function add(string $subject, string $message, string $recipient_name, string $recipient_mail) : Mail
    {
        $this->data->subject        = $subject;
        $this->data->body           = $message;
        $this->data->recipient_name = $recipient_name;
        $this->data->recipient_mail = $recipient_mail;

        return $this;
    }

    public function attach(string $filePath, string $fileName): Mail
    {
        $this->data->attach[$filePath] = $fileName;
        return $this;
    }

    public function send(string $fromName = CONFIG_SMTP_MAIL['fromName'], string $fromMail = CONFIG_SMTP_MAIL['fromMail']): bool
    {
        try {
            $this->phpmailer->Subject = $this->data->subject;
            $this->phpmailer->msgHTML($this->data->body);
            $this->phpmailer->addAddress($this->data->recipient_mail, $this->data->recipient_name);
            $this->phpmailer->setFrom($fromMail, $fromName);

            if(!empty($this->data->attach)) {
                foreach($this->data->attach as $patch => $name){
                    $this->phpmailer->addAttachment($patch, $name);
                }
            }

            $this->phpmailer->send();
            return true;
        } catch (Exception $exception) {
            $this->error = $exception;
            return false;
        }
    }

    public function error(): ?Exception
    {
        return $this->error;
    }
}