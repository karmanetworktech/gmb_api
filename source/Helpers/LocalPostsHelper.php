<?php


namespace Source\Helpers;


class LocalPostsHelper
{

    public $localPostData;

    public function __construct($localPostData = null)
    {
        $this->localPostData = $localPostData;
    }

    public function generateJsonLocalPostData()
    {
        $postInfo = $this->localPostJsonDefault();
        $postInfo["topicType"] = $this->localPostData->topicType;

        return($postInfo);
    }

    //This following code is default to all local post type
    public function localPostJsonDefault()
    {
        $mediaItems = array();
        if(isset($this->localPostData->media) && sizeof($this->localPostData->media) > 0) {
            foreach($this->localPostData->media as $mediaItem) {
                $images = array("mediaFormat" => $mediaItem->mediaFormat, "sourceUrl" => $mediaItem->googleUrl);
                array_push($mediaItems, $images);
            }
        }

        $postData = array(
            "languageCode"  => $this->localPostData->languageCode,
            "summary"       => $this->localPostData->summary
        );

        //Add Images
        if(sizeof($mediaItems) > 0) {
            $postData["media"] = $mediaItems;
        }

        //Add call to actions
        if(isset($this->localPostData->callToAction)) {
            $postData["callToAction"] = $this->localPostData->callToAction;
        }

        if($this->localPostData->topicType == 'EVENT') {
            $postData["event"] = $this->localPostData->event;
        }

        return $postData;
    }
}