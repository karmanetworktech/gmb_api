<?php


namespace Source\Helpers;

use DateTime;

class TokenIsValid
{
    public function testTokenValidate(): bool
    {
        date_default_timezone_set('Europe/lisbon');
        $current_time   = new DateTime();
        $current_time   = strtotime($current_time->format('H:i:s'));

        if(isset($_SESSION['time_to_refresh'])) {
            $tokenIsValidAt = strtotime($_SESSION['time_to_refresh']);
            return $current_time < $tokenIsValidAt;
        }
        
        return false;
    }
}