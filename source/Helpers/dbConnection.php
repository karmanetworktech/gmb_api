<?php


namespace Source\Helpers;
use CoffeeCode\DataLayer\Connect;

class dbConnection
{
    private Connect $connector;
    private Connect $error;

    /**
     * dbConnection constructor.
     * @param $conn
     */
    public function __construct()
    {
        $this->connector = Connect::getInstance();
        $this->error = Connect::getError();
    }

    /**
     * @return Connect|\PDO|null
     */
    public function getConn()
    {
        if($this->error) {
            return $this->error->getMessage();
        } else {
            var_dump(true);
        }
    }
}