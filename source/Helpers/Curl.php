<?php


namespace Source\Helpers;


class Curl
{
    private $url;
    private $method;

    public function __construct($url, $method)
    {
        $this->url = $url;
        $this->method = $method;
    }

    function executeCurl($body = null) {

        try {
            $curl = curl_init();
            $token= unserialize( $_SESSION['google_token']);

            curl_setopt_array($curl, [
                CURLOPT_URL             => $this->url,
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_TIMEOUT         => 30,
                CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST   => $this->method,
                CURLOPT_HTTPHEADER      => array( "Authorization: Bearer " . $token['access_token'] )
            ]);

            if(null != $body)  {
                curl_setopt_array($curl, [
                    CURLOPT_POSTFIELDS => $body,
                    CURLOPT_HTTPHEADER => [
                        "content-type: application/json",
                        "Authorization: Bearer " . $token['access_token']
                    ]
                ] );
            }

            $response = curl_exec($curl);
            $error = curl_error($curl);

            if($error) { echo $error;  die;}

            return $response;

        }
        catch (\Throwable $th) {
            echo $th->getMessage();
            die;
        }


    }
}