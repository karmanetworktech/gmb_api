<?php
$v->layout("_theme");

$list = json_decode($list_account);
?>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb mt-1">
        <li class="breadcrumb-item"><a href="<?= ROOT ?>">Home</a></li>
        <li class="breadcrumb-item active" aria-current="page">Accounts</li>
    </ol>
</nav>

<div class="row mb-4">
    <?php
    $img = ROOT . "/theme/assets/img/location.gif";

    if ($list->accounts) { 
        foreach ($list->accounts as $account) { ?>
            <div class="col-lg-3 account">
                <div class="card mb-3 account">
                    <div class="image">
                        <img class="card-img-top" src="<?= $img; ?>" alt="Card image cap" loading="lazy">
                        <h5 class="card-title"><?= $account->accountName; ?></h5>
                    </div>

                    <div class="card-footer">
                        <?php $account_id = explode('/', $account->name)[1]; ?>

                        <a href="<?= ROOT."/accounts/{$account_id}/locations"; ?>" class="btn btn-kgmb">Locations</a>
                        <div class="dropdown">
                            <button type="button" class="btn btn-kgmb dropdown-toggle" id="post_options" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Posts Options
                            </button>
                            <div class="dropdown-menu" aria-labelledby="post_options">
                                <a href="<?= ROOT."/accounts/{$account_id}/locations/localPost"; ?>" class="dropdown-item">List Posts</a>
                                <button data-account="<?= $account_id ?>" class="account_new_post dropdown-item add_new_post">Add new post</button>
                                <button data-account="<?= $account_id ?>" class="account_new_post alertPost dropdown-item add_new_post">Add new alert</button>
                                <button data-account="<?= $account_id ?>" class="dropdown-item list-sheets">Add posts from sheet</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        <?php }
    } ?>
</div>

<div class="modal" tabindex="-1" id="modal_sheets" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title col-lg-4">Sheet selection</h5>
                <input type="text" class="form-control" name="sheetNameFilter" id="sheetNameFilter">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body"></div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="modal_sheets_columns_select" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Sheets columns selector</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <div class="row flex-wrap">
                    <div class="mb-3 input-group">
                        <label for="link_img" class="input-group-text">Link image</label>
                        <select class="form-control" id="link_img"></select>
                    </div>

                    <div class="mb-3 input-group">
                        <label for="post_text" class="input-group-text">Post Text</label>
                        <select class="form-control" id="post_text"></select>
                    </div>

                    <div class="mb-3 input-group">
                        <label for="action_link" class="input-group-text">Action link</label>
                        <select class="form-control" id="action_link"></select>
                    </div>

                    <div class="mb-3 col-lg-6">
                        <label for="filter_date" class="form-label">Filter date by</label>
                        <select class="form-control fix" id="filter_date">
                            <option value="all">All</option>
                            <option value="today">Today</option>
                        </select>
                    </div>

                    <div class="mb-3 col-lg-6 form-group hide">
                        <label for="date_column" class="form-label">Column date</label>
                        <select class="form-control" id="date_column"></select>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button id="post_sheet_preview" class="btn btn-kgmb-inverse">Preview</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" id="modal_posts_to_create" data-bs-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Posts preview</h5>
                <div class="multi-selector locations_list col-lg-6">
                    <select class="form-select" multiple id="locationsList" name="locationsList" required></select>
                    <div class="selector-container my-0 w-100">
                        <input type="text" role="listbox" class="selector_value">
                    </div>
                </div>
                
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button id="btn_create_posts" class="btn btn-kgmb-inverse">Create posts</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="createPostByType" tabindex="-1" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="mb-0"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <div class="mb-3 multi-selector">
                    <select class="form-select" multiple id="locationsList" name="locationsList" required></select>
                    <div class="selector-container mt-0">
                        <input type="text" role="listbox" class="selector_value" data-selector="teste">
                    </div>
                </div>

                <!-- Formulário para post do tipo standard -->
                <form id="createPostToLocations" data-type="STANDARD" id="createPostToLocations">
                    <div class="mb-3">
                        <label for="imageUrl" class="form-label">Image URL</label>
                        <input type="text" class="form-control" name="imageUrl" id="imageUrl" required>
                    </div>
    
                    <div class="mb-3">
                        <label for="postSummary" class="form-label">Post text</label>
                        <textarea name="postSummary" class="form-control" id="postSummary" rows="5"></textarea>
                    </div>
    
                    <div class="mb-3">
                        <label for="actionUrl" class="form-label">CTA</label>
                        <input type="text" class="form-control" name="actionUrl" id="actionUrl" required>
                    </div>
                    <button type="submit" class="btn btn-kgmb-inverse">Create post</button>
                </form>
                <!-- Formulário para post do tipo standard -->

                <!-- Formulário para post do tipo standard -->
                <form class="hide" data-type="ALERT" id="createAlertToLocations" name="createAlertToLocations">
                    <div class="mb-3">
                        <label for="postSummary" class="form-label">Post text</label>
                        <textarea name="postSummary" class="form-control" id="postSummary" rows="5"></textarea>
                    </div>

                    <div class="mb-3">
                        <label for="actionUrl" class="form-label">CTA</label>
                        <input type="text" class="form-control" name="actionUrl" id="actionUrl" required>
                    </div>

                    <button type="submit" class="btn btn-kgmb-inverse">Create alert</button>
                </form>
                <!-- Formulário para post do tipo standard -->
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="relaunchPost" tabindex="-1" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Relaunch posts</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <div class="mb-3 multi-selector">
                    <select class="form-select" multiple id="locationsListRelaunch" name="locationsList"></select>
                    <div class="selector-container mt-0">
                        <input type="text" role="listbox" class="selector_value" data-selector="teste">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="PostRelaunch">Relaunch</button>
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $v->start('scripts') ?>
<script src="<?= ROOT; ?>/theme/assets/js/accounts.js" type="module"></script>
<script src="<?= ROOT; ?>/theme/assets/js/multiselector.js"></script>
<script type="text/javascript">
    var account = '';
</script>
<?php $v->end('scripts') ?>