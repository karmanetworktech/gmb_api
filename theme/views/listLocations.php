<?php $v->start('styles') ?>
    <link rel="stylesheet" href="<?= ROOT."/theme/assets/css/location.min.css"; ?>">
<?php $v->end('styles') ?>

<?php $v->layout("_theme"); ?>
<div aria-label="breadcrumb">
    <ol class="breadcrumb mt-1">
        <li class="breadcrumb-item"><a href="<?= ROOT ?>">Home</a></li>
        <li class="breadcrumb-item"><a href="<?= ROOT . "/listAccounts"; ?>">Accounts</a></li>
        <li class="breadcrumb-item active" aria-current="page"><?= $account_info->accountName; ?></li>
    </ol>
</div>

<?php

    if(!isset($locations_list)) :?>
        <input type="hidden" name="account" id="account" value="<?= $account; ?>">
        <div class="col-lg-5">
            <div class="input-group mb-3 col-lg-5">
                <label class="input-group-text" for="inputGroupSelect01">Location status</label>
                <select class="form-select"  id="locations_filter">
                    <option value="ALL" selected>All</option>
                    <option value="OPEN">Open</option>
                    <option value="CLOSED_PERMANENTLY">Closed</option>
                    <option value="NOT_VERIFIED">Not verified</option>
                </select>
            </div>
        </div>
        <section id="list_locations"></section>
    
    <?php elseif(sizeof($locations_list) == 0) : ?>
        <section id="list_locations" class="mb-4">
            <span>This location don´t have posts!</span>
        </section>
        
    <?php else : ?>
        <input type="hidden" name="account" id="account" value="<?= $account; ?>">
        <div class="col-lg-5">
            <div class="input-group mb-3 col-lg-5">
                <label class="input-group-text" for="inputGroupSelect01">Location status</label>
                <select class="form-select"  id="locations_filter">
                    <option value="ALL" selected>All</option>
                    <option value="OPEN">Open</option>
                    <option value="CLOSED_PERMANENTLY">Closed</option>
                    <option value="NOT_VERIFIED">Not verified</option>
                </select>
            </div>
        </div>

        <?php $v->insert('fragment/locationList', ['locations_list' => $locations_list]); ?>
    <?php endif;
?>

<!-- Locations options -->
<div class="d-flex mb-5 border-top border-dark pt-4 location-options">
    <button class="btn btn-kgmb-inverse postPopup">Create post</button>
    <button class="btn btn-kgmb-inverse postPopup alertPost">Create Alert</button>
    <button class="btn btn-kgmb-inverse" id="location_create_post_sheet" data-account="<?= $account; ?>" >Add posts from sheet</button>
    <button class="btn btn-kgmb-warn posts_relaunch" data-bs-target="#modal_posts_relaunch" data-bs-toggle="modal">Relaunch posts</button>
    <div class="alert alert-danger mb-0 ms-4 py-2 d-flex align-items-center">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
            <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
        </svg>
        <div>This actions affect all locations that are open!</div>
    </div>
</div#>
<!-- Locations options -->

<div class="modal fade" id="createPostByType" tabindex="-1" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5>COVID 19 ALERT POST</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <div class="mb-3 multi-selector">
                    <select class="form-select" multiple id="locationsList" name="locationsList"></select>
                    <div class="selector-container mt-0">
                        <input type="text" role="listbox" class="selector_value">
                    </div>
                </div>

                <!-- Formulário para post do tipo standard -->
                <form data-type="STANDARD" id="createPost">
                    <div class="mb-3">
                        <label for="imageUrl" class="form-label">Image URL</label>
                        <input type="text" class="form-control" name="imageUrl" id="imageUrl" required>
                    </div>
    
                    <div class="mb-3">
                        <label for="postSummary" class="form-label">Post text</label>
                        <textarea name="postSummary" class="form-control" id="postSummary" rows="5"></textarea>
                    </div>
    
                    <div class="mb-3">
                        <label for="actionUrl" class="form-label">CTA</label>
                        <input type="text" class="form-control" name="actionUrl" id="actionUrl" required>
                    </div>

                    <button type="submit" class="btn btn-kgmb-inverse">Create post</button>
                </form>
                <!-- Formulário para post do tipo standard -->

                <!-- Formulário para post do tipo standard -->
                <form class="hide" data-type="ALERT" id="createAlert">
                    <div class="mb-3">
                        <label for="postSummary" class="form-label">Post text</label>
                        <textarea name="postSummary" class="form-control" id="postSummary" rows="5"></textarea>
                    </div>

                    <div class="row">
                        <div class="mb-3">
                            <label for="actionUrl" class="form-label">CTA</label>
                            <input type="text" class="form-control" name="actionUrl" id="actionUrl" required>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-kgmb-inverse">Create alert</button>
                </form>
                <!-- Formulário para post do tipo standard -->
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" id="modal_sheets" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title col-lg-4">Sheet selection</h5>
                <input type="text" class="form-control" name="sheetNameFilter" id="sheetNameFilter">
                <button type="button" class="btn-close ms-3" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="modal_sheets_columns_select" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Sheets columns selector</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <div class="row flex-wrap">
                    <div class="mb-3 input-group">
                        <label for="link_img" class="input-group-text">Link image</label>
                        <select class="form-control" id="link_img"></select>
                    </div>

                    <div class="mb-3 input-group">
                        <label for="post_text" class="input-group-text">Post Text</label>
                        <select class="form-control" id="post_text"></select>
                    </div>

                    <div class="mb-3 input-group">
                        <label for="action_link" class="input-group-text">Action link</label>
                        <select class="form-control" id="action_link"></select>
                    </div>

                    <div class="mb-3 col-lg-6">
                        <label for="filter_date" class="form-label">Filter date by</label>
                        <select class="form-control fix" id="filter_date">
                            <option value="all">All</option>
                            <option value="today">Today</option>
                        </select>
                    </div>

                    <div class="mb-3 col-lg-6 form-group hide">
                        <label for="date_column" class="form-label">Column date</label>
                        <select class="form-control" id="date_column"></select>
                    </div>
                </div>

                <button type="button" id="post_sheet_preview" class="btn btn-kgmb-inverse">Preview</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" tabindex="-1" id="modal_posts_to_create" data-bs-backdrop="static">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Posts preview</h5>
                <div class="multi-selector locations_list col-lg-6">
                    <select class="form-select" multiple id="locationsList" name="locationsList"></select>
                    <div class="selector-container my-0 w-100">
                        <input type="text" role="listbox" class="selector_value">
                    </div>
                </div>

                <div class="mb-3 multi-selector"></div>

                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button data-account="" id="btn_create_posts" class="d-none">Create posts</button>
                <button type="button" data-account="" id="location_create_posts_from_sheet" class="btn btn-kgmb-inverse">Create posts</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" id="modal_posts_relaunch" data-bs-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Posts relaunch</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">
                <div class="mb-3 multi-selector">
                    <select class="form-select" multiple id="locationsList" name="locationsList"></select>
                    <div class="selector-container mt-0">
                        <input type="text" role="listbox" class="selector_value">
                    </div>
                </div>
            
                <button id="location_relaunch_posts" data-account="<?= $account; ?>" class="btn btn-kgmb">Relaunch posts</button>
            </div>
        </div>
    </div>
</div>

<?php $v->start('scripts') ?>
    <script src="<?= ROOT; ?>/theme/assets/js/locations.js" type="module"></script>
    <script src="<?= ROOT; ?>/theme/assets/js/multiselector.js" type="module"></script>
    <script>const account = "<?= $account; ?>"</script>
<?php $v->end('scripts') ?>