<?php $v->layout("_theme"); ?>

<?php 
    $client = null;
    if(isset($_SESSION['client_info'])) : $client = $_SESSION['client_info']; endif; 
?>

<div class="container mt-4 px-0">
    <div class="jumbotron">
        <h1 class="display-4">Google My BUsiness API!</h1>
        <p class="lead">Tools to manage your GMB account.</p>
        <hr class="my-4">
        <p>Accounts</p>
        <?php if($client) : ?>
            <a class="btn btn-accounts btn-lg" href="<?= GOOGLE['redirectUri'].'/listAccounts' ?>" role="button">List your accounts</a>
        <?php else : ?>
            <span>
                Efetue o
                <a href="<?= $login_url; ?>">login</a>
                para poder listar as suas contas GMB!
            </span>
        <?php endif; ?>
    </div>
</div>

<script>
    const page_config = <?= json_decode(json_encode($configs)); ?>;
</script>