<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= $title ?></title>

    <!-- CSS INIT -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= ROOT.'/theme/assets/css/main.min.css'?>">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>

    <?php if($v->section('styles')) : echo $v->section('styles'); endif; ?>
    <!-- CSS FINAL -->
</head>
    <body>
        <header>
            <?php
                $client = null;
                if(isset($_SESSION['client_info'])) : $client = $_SESSION['client_info'];   endif;

                if($v->section("menu")) :
                    echo $v->section("menu");
                else : ?>
                    <nav class="navbar navbar-light bg-light">
                        <div class="container">
                            <a class="navbar-brand" href="<?= GOOGLE['redirectUri'] ?>">
                                <img src="https://cdn.worldvectorlogo.com/logos/google-my-business-logo.svg" width="40" height="40" alt="" loading="lazy">
                                <span class="ms-2"><?= SITE; ?></span>
                            </a>

                            <?php
                                if($client) {
                                    $client = ( json_decode( json_encode($client)) );  ?>
                                    <span>Login válido até: <?= $_SESSION['time_to_refresh'] ?></span>
                                    <div class="navbar-text user-info">
                                        <img class="user-img" src="<?= ROOT; ?>/theme/assets/img/user.png" alt="User image">
                                        <div class="balloon">
                                            <div class="balloon-container">
                                                <h2><?= $client->name ?></h2>
                                                <p><?= $client->email ?></p>
                                                <hr>

                                                <a href="<?= GOOGLE['redirectUri'].'/login?off=true'; ?>">Logout</a>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                                else { ?>
                                    <a class="btn-login" href="<?= $login_url; ?>">
                                        <img width="24px" height="24px" src="<?= ROOT . '/theme/assets/img/google.png'; ?>" alt="Google Login">
                                        <span>Google</span>
                                    </a>
                                <?php }
                            ?>
                        </div>
                    </nav>
                <?php endif;
            ?>

        </header>

        <main class="container">
            <?php
                if( $v->section('content') ) :
                    echo $v->section('content');
                endif;
            ?>
        </main>

        <footer></footer>

    <!-- JS INIT -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="<?= ROOT.'/theme/assets/js/main.js'; ?>" type="module"></script>
    <script src="<?= ROOT.'/theme/assets/js/siteConfig.js'; ?>" type="module"></script>
    <script src="<?= ROOT.'/theme/assets/js/functions.js'; ?>" type="module"></script>
    <script type="text/javascript">const root_url = "<?= ROOT; ?>";</script>
    <!-- JS FINAL -->
    </body>

    <?php if($v->section('scripts')) : echo $v->section('scripts'); endif; ?>
</html>