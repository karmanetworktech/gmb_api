<?php $v->layout("_theme"); ?>

<?php
    if ($list) : ?>
        <div class="cards" style="display: flex">
        <?php
            foreach ($list as $item) : ?>

                <div class="card" style="width: 18rem; margin: 0 10px">
                    <img style="width: 100%" src="<?= $item->media[0]->googleUrl ?>" class="card-img-top" alt="Post Image">
                    <div class="card-body">
                        <p class="card-text"><?= $item->summary ?></p>
                        <!--a href="#" class="btn btn-primary">Go somewhere</a-->
                    </div>
                </div>

            <?php endforeach; ?>
        </div>
    <?php else : ?>
        <h4>List of local post not informed!</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, obcaecati!</p>
    <?php endif;