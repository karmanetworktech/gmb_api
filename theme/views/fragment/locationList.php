<?php if ($locations_list) : ?>
    <section id="list_locations">
        <ul class="locations-list row flex-wrap p-0">
            <?php foreach ($locations_list as $location) : ?>
                <?php
                    $img        = ROOT . "/theme/assets/img/post.gif";
                    $location_id = explode('/', $location->name)[1];
                    $phone      = isset($location->primaryPhone) ? $location->primaryPhone : '---';
                    $status     = $location->openInfo->status;
                    $postsLink  = ROOT . "/accounts/{$account}/location/{$location_id}/localPost";
                    $address    = isset($location->storefrontAddress->addressLines) ? $location->storefrontAddress->addressLines[0] : '---';

                    if (isset($location->websiteUri)) {
                        $link = strlen($location->websiteUri) > 35 ? substr($location->websiteUri, 0, 35) . '[...]' : $location->websiteUri;
                    } else {
                        $link = '---';
                    }
                ?>
                <li class="col-lg-3" data-status="<?= $status; ?>">
                    <article class="card location">
                        <div class="image">
                            <img src="<?= $img ?>" class="card-img-top" alt="...">
                            <div class="details">
                                <h3 class="location-name"><?= $location->title ?></h3>
                            </div>
                        </div>

                        <div class="card-body">
                            <ul class="location-info">
                                <li class="status">
                                    <i class="fas fa-exclamation"></i>
                                    <p><?= str_replace('_', ' ', $status); ?></p>
                                </li>

                                <li class="address">
                                    <i class="fas fa-map-pin"></i>
                                    <p><?= $address; ?></p>
                                </li>

                                <li class="category">
                                    <i class="fas fa-info"></i>
                                    <p><?= $location->categories->primaryCategory->displayName; ?></p>
                                </li>

                                <li class="phone">
                                    <i class="fas fa-phone"></i>
                                    <p><?= $phone; ?></p>
                                </li>

                                <li class="site">
                                    <i class="fas fa-at"></i>
                                    <a href="<?= $location->websiteUri; ?>"><?= $link; ?></a>
                                </li>
                            </ul>

                            <?php if ($status == 'OPEN') : ?>
                                <a href="<?= $postsLink ?>" class="btn btn-kgmb-inverse mt-4">Post list</a>
                            <?php endif; ?>
                        </div>
                    </article>
                </li>
            <?php endforeach; ?>
        </ul>

        <?php if (intval($prev_page_num) >= 0) : ?>
            <?php
            $prevPageLink = isset($_SESSION['locations_paginate_tokens'][$prev_page_num]) ?
                $router->route("list.locations") . "?next_page={$_SESSION['locations_paginate_tokens'][$prev_page_num]}" :
                $router->route("list.locations");
            $prevPageLink = str_replace('{account}', $account, $prevPageLink);
            ?>

            <button id="next-link" class="btn btn-kgmb-inverse mb-4" data-page="<?= $prev_page_num; ?>" data-action="<?= $prevPageLink; ?>">
                Previous page
            </button>
        <?php endif; ?>

        <?php if ($next_page) : ?>
            <?php
            $nextPageLink = $router->route("list.locations") . "?next_page=$next_page";
            $nextPageLink = str_replace('{account}', $account, $nextPageLink);
            ?>
            <button id="next-link" class="btn btn-kgmb-inverse mb-4" data-page="<?= $next_page_num ?>" data-action="<?= $nextPageLink ?>">
                Next page
            </button>

        <?php endif; ?>

    </section>

<?php else : ?>
    <section id="list_locations" class="mb-4">
        <span>This location don´t have posts!</span>
    </section>
<?php endif; ?>