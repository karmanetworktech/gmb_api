// *************************************  SHEET FILTER  ************************************* //
const sheetFilter = document.querySelector('#sheetNameFilter');
var sheetList = null;
if (sheetFilter) {
    sheetFilter.addEventListener('focus', (el) => {
        el.preventDefault();

        sheetList = [...document.querySelectorAll('.sheets-list .sheet-item')];
        sheetFilter.value = '';
    });

    sheetFilter.addEventListener('input', (input) => {
        input.preventDefault();

        sheetList.forEach(item => {
            var text = input.currentTarget.value.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
            var textItem = item.textContent.toLowerCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");

            if (!textItem.includes(text)) {
                item.classList.add('hide');
            } else {
                item.classList.remove('hide');
            }
        });
    });
}
// *************************************  SHEET FILTER  ************************************* //


// *******************************  Show user info balloon ******************************* //
const user_img = document.querySelector('.user-img');
if (user_img) {
    user_img.addEventListener('click', user => {
        let father_element = user.currentTarget.parentElement;
        let balloon = father_element.querySelector('.balloon');

        if (balloon.style.opacity == 1) {
            balloon.style.opacity = 0;
            setTimeout(() => { balloon.classList.remove('show') }, 500);
        }
        else {
            balloon.classList.add('show');
            setTimeout(() => { balloon.style.opacity = 1; }, 20);
        }
    });
}
// *******************************  Show user info balloon ******************************* //