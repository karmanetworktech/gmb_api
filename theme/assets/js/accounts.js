import Account from './modules/Account.js';
import LocationFilter from './modules/LocationFilter.js';
import {Loading}    from "../js/modules/userIteraction.js";
import LocalPost from "../js/modules/LocalPost.js";

const accounts_new_post = document.querySelectorAll('.account_new_post');
if(accounts_new_post) {
    accounts_new_post.forEach(bnt_new_post => {
        bnt_new_post.addEventListener('click', async (btn) => {
            btn.preventDefault();
            var loading = new Loading();
            loading.addLoading();

            var account_id = btn.currentTarget.dataset.account;
                account = account_id;
            var locationsSelect = document.querySelector('#createPostByType #locationsList');

            if(btn.currentTarget.classList.contains('alertPost')) {
                document.querySelector('[data-type="STANDARD"]').classList.add('hide');
                document.querySelector('[data-type="ALERT"]').classList.remove('hide');
                document.querySelector('#createPostByType .modal-header h5').textContent = 'COVID 19 ALERT POST';
            } else {
                document.querySelector('[data-type="STANDARD"]').classList.remove('hide');
                document.querySelector('[data-type="ALERT"]').classList.add('hide');
                document.querySelector('#createPostByType .modal-header h5').textContent = 'DEFAULT POST';
            }

            await new LocationFilter(account_id, locationsSelect).listLoctaionSelect();
            loading.remove_loading();
            
            var myModal = new bootstrap.Modal(document.querySelector('#createPostByType'));
            myModal.toggle();
        });
    }); 
}

const CreatePostsToLocations = document.querySelector('#createPostToLocations');
if(CreatePostsToLocations) { 
    CreatePostsToLocations.addEventListener('submit', async (form) => {
        form.preventDefault();

        var loading = new Loading();
        loading.addLoading();

        var options = CreatePostsToLocations.parentElement.querySelector('#locationsList').selectedOptions;
            options = [... options].filter(value => value.value != 'all');
            
        if(!options || options.length === 0) { 
            loading.remove_loading();
            alert('Select a location from the list!'); 
            return; 
        }

        var locations = [... options].map(({ value }) => { return value.split('/')[1]; });
        var data = new FormData(CreatePostsToLocations);

        var info = {
            account, locations,
            posts: {
                post : {
                    languageCode: 'en_US',
                    summary     : data.get('postSummary'),
                    topicType   : "STANDARD", 
                    callToAction: { actionType: "LEARN_MORE", url: data.get('actionUrl') },
                    media       : [{ mediaFormat: "PHOTO", sourceUrl: data.get('imageUrl') }]
                }
            }
        }

        var post = new LocalPost();
        await post.creatPost(info);
        window.location.reload();
    })
}

const CreatePostsToAllLocations = document.querySelector('#createPostToAllLocations');
if(CreatePostsToAllLocations) { 
    CreatePostsToAllLocations.addEventListener('submit', async (form) => {
        form.preventDefault();

        var loading = new Loading();
        loading.addLoading();

        var account = document.querySelector('#account').value;
        var options = form.currentTarget.parentElement.querySelector('#locationsList').selectedOptions;
            options = [... options].filter(value => value.value != 'all');
        
        if(!options || options.length === 0) { 
            loading.remove_loading();
            alert('Select a location from the list!'); 
            return; 
        }

        var locations = [... options].map(({ value }) => { return value.split('/')[1]; });
        let data = new FormData(CreatePostsToAllLocations);

        var info = {
            account, locations,
            posts: {
                post : {
                    languageCode: 'en_US',
                    summary     : data.get('postSummary'),
                    topicType   : "STANDARD",
                    callToAction: { actionType: "LEARN_MORE", url: data.get('actionUrl') },
                    media       : [{ mediaFormat: "PHOTO", sourceUrl: data.get('imageUrl') }]
                }
            }
        }

        var post = new LocalPost();
        await post.creatPost(info);
        window.location.reload();
    });
}

const CreateAlertToALlLocations = document.querySelector('#createAlertToLocations');
if(CreateAlertToALlLocations) { 
    CreateAlertToALlLocations.addEventListener('submit', async (form) => {
        form.preventDefault();
        var loading = new Loading();
        loading.addLoading();

        var options = form.currentTarget.parentElement.querySelector('#locationsList').selectedOptions;
            options = [... options].filter(value => value.value != 'all');

        if(!options || options.length === 0) { 
            loading.remove_loading();
            alert('Select a location from the list!'); 
            return; 
        }

        var locations = [... options].map(({ value }) => { return value.split('/')[1]; });
        let data = new FormData(CreateAlertToALlLocations);

        var info = {
            account, locations,
            posts: {
                post : {
                    languageCode: 'en_US',
                    summary     : data.get('postSummary'),
                    topicType   : "ALERT",
                    alertType   : "COVID_19",
                    callToAction: { actionType: "LEARN_MORE", url: data.get('actionUrl') }
                }
            }
        }
        
        var post = new LocalPost();
        await post.creatPost(info);
        window.location.reload();
    })
}

/* =============================== Create local post com seleção do tipo de post =============================== //
const postByType = document.querySelector('#createPostByType');
if(postByType) {
    //Ajustar a view de acordo com o tipo de post selecionado
    const postTypeSelector = postByType.querySelector('.modal-header select');
    const postTypeSelectorForms = postByType.querySelectorAll('form');
    
    postTypeSelector.addEventListener('change', (el) => {
        postTypeSelectorForms.forEach(form => {
            if(form.dataset.type == el.currentTarget.value) {
                form.classList.remove('hide');
            } else {
                form.classList.add('hide');
            }
        });
    });
}
// =============================== Create local post com seleção do tipo de post =============================== */

// ============================================ Relauch post ============================================ //
const accounts_post_relaunch = document.querySelectorAll('#relauch_posts');
if(accounts_post_relaunch) {
    accounts_post_relaunch.forEach(bnt_new_post => {
        bnt_new_post.addEventListener('click', async (btn) => {
            btn.preventDefault();
            var loading = new Loading();
            loading.addLoading();

            var account_id = btn.currentTarget.dataset.account;
            var locationsSelect = document.querySelector('#relaunchPost #locationsList');
            var account = btn.currentTarget.dataset.account
            
            await new LocationFilter(account_id, locationsSelect).listLoctaionSelect();
            loading.remove_loading();

            var myModal = new bootstrap.Modal(document.querySelector('#relaunchPost'));
            myModal.toggle();
        });
    }); 
}

const btnRelaunchPost = document.querySelectorAll('#PostRelaunch');
if (btnRelaunchPost) {
    btnRelaunchPost.forEach(btn => {
        btn.addEventListener('click', async btn => {
            var loading = new Loading();
            loading.addLoading();
            
            btn.preventDefault();
            var btnInfo = btn.currentTarget;

            var options     = document.querySelector('#relaunchPost #locationsList').selectedOptions;
                options = [... options].filter(value => value.value != 'all');
            
            if(!options || options.length === 0) { 
                loading.remove_loading();
                alert('Select a location from the list!'); 
                return; 
            }

            var locations = [... options].map(({ value }) => { return value.split('/')[1]; });

            //Obter posts das localizações selecionadas
            var localPost = new LocalPost();
            await localPost.getPostAccountLocations({account, locations});

            window.location.reload();
        });
    });
}
// ============================================ Relauch post ============================================ //