import LocalPost from "../js/modules/LocalPost.js";
import GoogleSheets from "../js/modules/GoogleSheets.js";
import { Loading } from "../js/modules/userIteraction.js";
import AlertPost from "../js/modules/AlertPost.js";

const loading = new Loading();

/* =============== Criar novo post único para unica localização  =============== */
const createPostForm = document.querySelector('#postCreateButton');
if (createPostForm) {
    createPostForm.addEventListener('submit', async (el) => {
        loading.addLoading()
        el.preventDefault();

        let form = new FormData(createPostForm);
        let data = {
            languageCode    : 'en_US',
            summary         : form.get('postSummary'),
            topicType       : "STANDARD",
            callToAction    : { actionType: 'LEARN_MORE', url: form.get('actionUrl') },
            media           : [ { mediaFormat: "PHOTO", sourceUrl: form.get('imageUrl') } ]
        };

        let info = {
            account: form.get('account'),
            location: form.get('location'),
            posts: [ data ]
        };

        var post = new LocalPost();

        await post.creatPost(info);
        window.location.reload();
    });
}
/* =============== Criar novo post único para unica localização  =============== */


/* ================= Remover post único para unica localização  ================= */
const all_btn_post_delete = document.querySelectorAll('.post-delete');
if (all_btn_post_delete) {
    all_btn_post_delete.forEach(btn_delete => {
        btn_delete.addEventListener('click', async (button) => {
            let post_card = button.currentTarget.parentElement.parentElement;
            let data = {
                account: post_card.dataset.account,
                location: post_card.dataset.location,
                posts: [{ post_id: post_card.dataset.post, summary: post_card.querySelector('.summary').dataset.text }]
            }
            
            let localPost = new LocalPost();

            if (confirm('Would you really like to remove this post?')) {
                loading.addLoading();
                await localPost.removeLocalPost(data);
                window.location.reload();
            }
        });
    });
}

const btn_delete_all_posts = document.querySelector('#remove_location_post');
if (btn_delete_all_posts) {
    btn_delete_all_posts.addEventListener('click', async (button) => {
        let allPosts = document.querySelectorAll('.cards .localPost');
        let allPostsId = [];

        if (allPosts.length > 0) {
            allPosts.forEach(post => {
                allPostsId.push({ 
                    post_id: post.dataset.post, 
                    summary: post.querySelector('.summary').dataset.text
                })
            });

            let data = {
                account: document.querySelector('#account').value,
                location: document.querySelector('#location').value,
                posts: allPostsId
            }

            let localPost = new LocalPost();

            if (confirm('Would you really like to remove all post from this location?')) {
                loading.addLoading();
                await localPost.removeLocalPost(data);
                window.location.reload();
            }
        }
    });
}
/* ================= Remover post único para unica localização  ================= */

/* ================= Relançar post único para unica localização  ================= */
const btn_relaunch_post = document.querySelectorAll('.post-relaunch');
if (btn_relaunch_post) {
    btn_relaunch_post.forEach(btn => {
        btn.addEventListener('click', btn => {
            loading.addLoading();
            btn.preventDefault();

            let post_card = btn.currentTarget.parentElement.parentElement;
            let post_action_info = post_card.querySelector('[name=action_info]')

            let post = {
                languageCode: 'en_US',
                summary: post_card.querySelector('.summary').dataset.text,
                topicType: "STANDARD",

                callToAction: {
                    actionType: post_action_info.dataset.type,
                    url: post_action_info.dataset.url
                },

                "media": [
                    {
                        "mediaFormat": "PHOTO",
                        "sourceUrl": post_card.querySelector('[name="post-image"]').value
                    }
                ]
            }


            let local_post = new LocalPost();
            local_post.post_list.push({
                account: post_card.dataset.account,
                location: post_card.dataset.location,
                posts: [{
                    post_id: post_card.dataset.post,
                    post_detail: post
                }]
            });

            local_post.relaunch_posts().then(() => {
                setTimeout(() => window.location.reload(), 2000);
            });
        });
    });
}

const btn_relaunch_all_post = document.querySelectorAll('#relaunch_location_post');
if (btn_relaunch_all_post) {
    btn_relaunch_all_post.forEach(btn => {
        btn.addEventListener('click', btn => {
            loading.addLoading();
            btn.preventDefault();

            let all_cards = document.querySelectorAll('.cards .localPost');
            let allPostData = [];

            if(all_cards.length > 0) {
                all_cards.forEach(card => {
                    let post_action_info = card.querySelector('[name=action_info]');

                    let post = {
                        languageCode: 'en_US',
                        summary: card.querySelector('.summary').dataset.text,
                        topicType: "STANDARD",
        
                        callToAction: {
                            actionType: post_action_info.dataset.type,
                            url: post_action_info.dataset.url
                        },
        
                        "media": [
                            {
                                "mediaFormat": "PHOTO",
                                "sourceUrl": card.querySelector('[name="post-image"]').value
                            }
                        ]
                    };

                    allPostData.push({
                        post_id: card.dataset.post,
                        post_detail: post
                    });
                });

            }
            
            let local_post = new LocalPost();
            local_post.post_list.push({
                account: document.querySelector('#account').value,
                location: btn.currentTarget.dataset.location,
                posts: allPostData
            });

            local_post.relaunch_posts().then(() => {
                setTimeout(() => window.location.reload(), 2000);
            });
        });
    });
}
/* ================= Relançar post único para unica localização  ================= */

// ==================================  List Sheets  ================================== //
const list_sheets_btn = document.querySelectorAll('.list-sheets');
const btn_create_post = document.querySelector('#modal_posts_to_create #btn_create_posts');
let posts_list = null;

if (list_sheets_btn) {
    list_sheets_btn.forEach(list_sheet => {
        list_sheet.addEventListener('click', async (el) => {
            loading.addLoading();

            let sheet = new GoogleSheets();
            let destiny = document.querySelector('#modal_sheets');

            sheet.account = document.querySelector('#account').value;
            sheet.location = document.querySelector('#location') ? document.querySelector('#location').value : '';

            await sheet.listAllGoogleSheets(destiny);

            loading.remove_loading();
        });
    })
}

if (document.querySelector('#post_sheet_preview') && document.querySelector('#location') ) {
    let item_info = document.querySelector('#post_sheet_preview');
    item_info.addEventListener('click', async () => {
        loading.addLoading();
        let columns = document.querySelectorAll('#modal_sheets_columns_select select');
        let destiny = document.querySelector('#modal_posts_to_create');
        let account = document.querySelector('#account').value;
        let location = document.querySelector('#location').value;

        let sheet = new GoogleSheets();
        await sheet.generatePreview(item_info, columns, destiny, account, location);
        posts_list = sheet.post_list;

        loading.remove_loading();
    })
}

if (btn_create_post && document.querySelector('#location')) {
    btn_create_post.addEventListener('click', async (btn) => {

        if (!btn.currentTarget.classList.contains('disabled')) {
            loading.addLoading();
            btn.preventDefault();
            let allPosts = btn.currentTarget.parentElement.parentElement.querySelectorAll('.post-item');
            let postsData = [];

            if(allPosts.length > 0) {
                allPosts.forEach(post => {
                    let data = {
                        languageCode: 'en_US',
                        summary: post.querySelector('.post-info h2').textContent,
                        topicType: "STANDARD",
        
                        callToAction: {
                            actionType: post.querySelector('.post-info a').textContent.toUpperCase().replace(' ', '_'),
                            url: post.querySelector('.post-info a').getAttribute('href')
                        },
        
                        "media": [
                            {
                                "mediaFormat": "PHOTO",
                                "sourceUrl": post.querySelector('img').getAttribute('src')
                            }
                        ]
                    }

                    postsData.push(data);
                });
            }

            let info = {
                account: document.querySelector('#account').value,
                location: document.querySelector('#location').value,
                posts: postsData
            };
            
            var post = new LocalPost();

            await post.creatPost(info);
            window.location.reload();
        }
    });
}

//Update filter value
const filter_option = document.querySelector('#filter_date');
if (filter_option) {
    filter_option.addEventListener('change', filter => {
        let el_father = filter_option.parentElement;
        if (filter.currentTarget.value == 'today') {
            el_father.classList.remove('col-md-12');
            el_father.classList.add('col-md-6');
            document.querySelector('#date_column').parentElement.classList.remove('hide');
        } else {
            el_father.classList.add('col-md-12');
            el_father.classList.remove('col-md-6');
            document.querySelector('#date_column').parentElement.classList.add('hide');
        }
    });
}

// ==================================  List Sheets  ================================== //

// =============================== Create local post ALERT =============================== //
const alert_form = document.querySelector('#alertCreate');
if(alert_form) {
    alert_form.addEventListener('submit', async (el) => {
        el.preventDefault();
        loading.addLoading();
        let account = document.querySelector('#account').value;
        let location= document.querySelector('#location').value

        let locations = [location];
        
        let data = new FormData(alert_form);
        var info = {
            account, locations,
            posts: {
                post : {
                    languageCode: 'en_US',
                    summary     : data.get('postSummary'),
                    topicType   : "ALERT",
                    alertType   : "COVID_19",
                    callToAction: { actionType: "LEARN_MORE", url: data.get('actionUrl') }
                }
            }
        }
        
        var post = new LocalPost();
        await post.creatPost(info);

        window.location.reload();
    });
}
// =============================== Create local post ALERT =============================== //