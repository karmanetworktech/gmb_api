import Fetch from "../modules/fetch.js";
import {ElementCreator, Loading} from "../modules/userIteraction.js";
import LocationFilter from "./LocationFilter.js";

class GoogleSheets {
    constructor(sheetId, account, location = '') {
        this.sheetId    = sheetId;
        this.account    = account;
        this.location   = location;
        this.fetch      = Fetch;
        this.post_list  = [];
    }

    listAllGoogleSheets = async (destiny) => {
        if(!destiny || destiny == '') {
            return 'The element destiny was\'t informed!';
        }

        else if(!this.account || this.account == '') {
            return 'Account not informed!';
        }

        else {
            let old_list = destiny.querySelector('.sheets-list');
            if (old_list) { old_list.remove(); }

            this.fetch.url = root_url + '/drive';
            this.fetch.method = 'GET';

            await this.fetch.execute_fetch()
                .then(response => response.json())
                .then(response => {
                    if (response.files.length > 0) {
                        let img_url = "//www.google.com/images/about/sheets-icon.svg";
                        let list_sheets = new ElementCreator('DIV', 'sheets-list').create_element();

                        response.files.forEach(file => {


                            let itemAttributes = this.account && this.account != '' ? 
                                                'id:' + file.id + ',data-account:' + this.account:
                                                'id:' + file.id;

                            let sheet_item = new ElementCreator('DIV', 'sheet-item', itemAttributes).create_element();

                            //let img = new ElementCreator('IMG', null, 'src:' + img_url, null, sheet_item)
                                //.create_element();

                            new ElementCreator('P', null, null, file.name, sheet_item)
                                .create_element();

                            list_sheets.append(sheet_item);

                            sheet_item.addEventListener('click', this.sheet_fields_select);
                        });

                        destiny.querySelector('.modal-body').append(list_sheets);
                        $(destiny).modal('show');
                    }
                })
                .catch(Error => alert(Error));
        }
    }

    sheet_fields_select = async (sheet_item) => {

        let loading = new Loading;
        loading.addLoading();

        this.sheet_id   = sheet_item.currentTarget.getAttribute('id');
        this.account    = sheet_item.currentTarget.dataset.account;
        this.location   = sheet_item.currentTarget.dataset.location;

        let modal_destiny = document.querySelector('#modal_sheets_columns_select');

        this.fetch.method = "GET";
        this.fetch.url = root_url + `/sheet/${this.sheet_id}`;

        await this.fetch.execute_fetch()
            .then(response => response.json())
            .then(response => {
                let element_creator = new ElementCreator();
                let options = modal_destiny.querySelectorAll('select');

                document.querySelector('#post_sheet_preview').dataset.account = this.account;
                document.querySelector('#post_sheet_preview').dataset.sheet = this.sheet_id;
                if(this.location) {
                    document.querySelector('#post_sheet_preview').dataset.location = this.location;
                }

                options.forEach(option => {
                    let label = option.parentElement.querySelector('label').textContent;
                    
                    let i = 0;

                    if(!option.classList.contains('fix')) {
                        if(option.querySelectorAll('option')) {
                            option.querySelectorAll('option').forEach(opt => { opt.remove(); });
                        }
                        response.values[0].forEach(item => {
                            var element = new ElementCreator('OPTION', null, 'value:' + i, item).create_element();

                            if(item.toLocaleLowerCase() == label.toLocaleLowerCase()) { 
                                element.selected = true;
                            };
                            option.append(element);
                            i++;
                        })
                    }
                })

                $('#modal_sheets').modal('hide')
                $('#modal_sheets_columns_select').modal('show');

                loading.remove_loading();
            })
    }

    generatePreview = async (btn, columns, destiny, account, location) => {
        this.account = account;
        this.location= location;
        this.sheet_id= btn.dataset.sheet;

        destiny.querySelector('#btn_create_posts').dataset.account = this.account;
        if(this.location && this.location !== "") {
            destiny.querySelector('#btn_create_posts').dataset.location = this.location;
        }

        let index = {};
        columns.forEach(item => { index[item.getAttribute('id')] = item.value ; });

        let url = this.location && this.location !== "" ?
                        `/accounts/${this.account}/location/${this.location}/sheet/${this.sheet_id}` :
                        `/accounts/${this.account}/sheet/${this.sheet_id}`;

        this.fetch.method = "POST";
        this.fetch.url = root_url + url;
        this.fetch.body = index;

        await this.fetch.execute_fetch()
            .then(response => response.json())
            .then(response => {
                $('#modal_sheets_columns_select').modal('hide');

                if(typeof response.locations == 'object' && response.locations.length > 0 && this.location === undefined) {
                    var locationsSelect = destiny.querySelector('#locationsList');
                    new LocationFilter(account, locationsSelect).listLoctaionSelect();
                }

                if(typeof response.posts == 'object' && response.posts.length > 0) {
                    this.post_preview(response.posts);

                    response.posts.forEach(post => { this.post_list.push(post); })
                }

                $(destiny).modal('show');

                return this.post_list;
            })
            .catch(Error => { alert(Error); });
    }

    post_preview = (data) => {
        let old_list = document.querySelectorAll('#modal_posts_to_create .modal-body .post-item');
        if(old_list) { old_list.forEach(old_item => { old_item.remove(); }); }

        data.forEach(post_data => {
            let action_link = post_data.callToAction.url;
            let action_type = post_data.callToAction.actionType;
            let description = post_data.summary;
            let image_url   = post_data.media.sourceUrl;

            let postType    = post_data.topicType == 'ALERT' ? 
                                'data-postType:'+post_data.topicType+'data-alertType:'+post_data.alertType : 
                                'data-postType:'+post_data.topicType

            let container = new ElementCreator('DIV', 'col-lg-3', postType).create_element();

            let post = new ElementCreator('DIV', 'post-item,card', postType).create_element();

            let imageContainer = new ElementCreator('DIV', 'image').create_element();

            let post_img = new ElementCreator('IMG', null, 'src:' + image_url).create_element();

            let post_info = new ElementCreator('DIV', 'post-info').create_element();
                new ElementCreator('H2', null, null, description, post_info).create_element();
                new ElementCreator('A', null, 'data-type:' + action_type + ',target:_blank,href:' + action_link, 'Learn more', post_info)
                .create_element();

            post.append(imageContainer);
            imageContainer.append(post_img);
            post.append(post_info);
            container.append(post);

            $('#modal_posts_to_create .modal-body').append(container);
            $('#modal_posts_to_create').modal('show');
            document.querySelector('#modal_posts_to_create .modal-body').classList.add('row');
        })
    }
}

export default GoogleSheets;