class Fetch {
    constructor(method, body, url) {
        this.method = method;
        this.body   = body;
        this.url    = url;
        this.header = { 'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8' };
    }

    execute_fetch () {
        if(this.method.toLowerCase() === 'get') {
            return fetch(this.url,{
                method  : this.method,
                headers : this.header
            });
        }
       return fetch(this.url,{
            method  : this.method,
            headers : this.header,
            body    :  JSON.stringify(this.body)
        });
    }
}


export default new Fetch;