import Fetch from "../modules/fetch.js";
import Alerts from "../modules/Alerts.js";

class LocalPost {

    constructor( account, location = '', post_data, list_locations = [], post_id = null, list_posts = null, post_to_clone = {}, post_list = [] ) {
        this.account        = account;
        this.location       = location;
        this.postData       = post_data;
        this.list_location  = list_locations;
        this.post_id        = post_id;
        this.list_posts     = list_posts;
        this.post_to_clone  = post_to_clone;
        this.post_list      = post_list;
        this.fetch          = Fetch;
    }

    removeLocalPost = async (info) => {
        if(!info) { return 'Account not informed'; }
        else {
            let url_to_delete = `/localpost/delete`;
            url_to_delete = root_url + url_to_delete;

            this.fetch.url = url_to_delete;
            this.fetch.method = 'DELETE';
            this.fetch.body = info;
            let output = "";

            await this.fetch.execute_fetch()
                .then(response => response.text())
                .then(response => {
                    let _alert   = new Alerts;
                    _alert.type  = 'success';
                    _alert.title = `The post ${info.posts[0].post_id} was successfully removed!`;
                    _alert.msg   = response;
                    _alert.show_alert();
                })

                .catch(error => { return error; });

            return output;
        }
    }

    removeAllLocalPostFromLocation = async () => {
        if(!this.account || this.account == '') { return 'Account not informed'; }

        else if(!this.location || this.location == '') { return 'Location not informed'; }

        else if(!this.list_posts || this.list_posts == '') { return 'Post list not informed'; }

        else {
            let url_to_delete = root_url + `/accounts/${this.account}/location/${this.location}/localPosts/delete`;

            this.fetch.url      = url_to_delete;
            this.fetch.method   = 'POST';
            this.fetch.body     = this.list_posts;
            let output = "";

            await this.fetch.execute_fetch()
                .then(response => response.text())
                .then(response => { output = response; })

                .catch(error => { return error; });

            return output;
        }

    }

    creatPost = async (data) => {
        if(!data) { return 'data not informed'; }
        else {
            let urlCreatePost = root_url + `/localPost/create`;

            this.fetch.url      = urlCreatePost;
            this.fetch.body     = data;
            this.fetch.method   = "POST";
            
            let output = await this.fetch.execute_fetch()
                .then(response => response.json())
                .then(response =>  { return response; } )
                .catch(error => { return error });

            return output;
        }
    }

    creatMultiplesPost = async () => {
        if(!this.account || this.account == '') { return 'Account not informed'; }

        else if(!this.list_location || this.list_location == '') { return 'Location list not informed'; }

        else if(!this.list_posts || this.list_posts == '') { return 'Post list not informed'; }

        else {
            let urlCreatePost = root_url + `/accounts/${this.account}/location/localPost/multiples`;
            let output;

            this.fetch.url      = urlCreatePost;
            this.fetch.body     = {'posts': this.list_posts, 'locations': this.list_location};
            this.fetch.method   = "POST";

            await this.fetch.execute_fetch()
                    .then(response => response.json())
                    .then(response => {
                       output = response;
                    });

            return output;
        }
    }

    relaunch_posts = async () => {
        let url_to_clone = root_url + '/localPost/relaunch';

        this.fetch.url      = url_to_clone;
        this.fetch.method   = 'POST';
        this.fetch.body     = this.post_list;

        await this.fetch.execute_fetch()
            .then(response => response.json())
            .then(response => { 
                let _alert   = new Alerts;
                _alert.type  = response.code == '200' ? 'success' : 'fail';
                _alert.msg   = response.msg;
                _alert.title = 'Post relaunch';
                _alert.show_alert();
            })
            .catch(error => { return error; });
    }

    getMetrics = async () => {
        let urlCreatePost = root_url + `/accounts/${this.account}/location/${this.location}/localPost/${this.post_id}/metrics`;

        this.fetch.url      = urlCreatePost;
        this.fetch.body     = this.postData;
        this.fetch.method   = "POST";
        this.body           = {date}

        await this.fetch.execute_fetch()
            .then(response => response.json())
            .then(response => {
                return response;
            })

            .catch(error => { return error; });

    }

    getPostAccountLocations = async (data) => {
        let url = root_url + `/accounts/locations/localPostListRelauch`;

        this.fetch.url      = url;
        this.fetch.body     = data;
        this.fetch.method   = "POST";
        
        let output = await this.fetch.execute_fetch()
            .then(response => response.json())
            .then(response =>  { return response; } )
            .catch(error => { return error });

        return output;
    }
}

export default LocalPost;