import Fetch from "../modules/fetch.js";

class AlertPost {
    constructor(account, location, summary, type, cta_type = null, cta_link = null) {
        this.account    = account;
        this.location   = location;
        this.summary    = summary;
        this.type       = type;
        this.cta_type   = cta_type;
        this.cta_link   = cta_link;
    }

    async registerAlertPost() {
        var fetch = Fetch;

        fetch.body = {
            account: this.account,
            location: this.location,
            posts: {
                post : {
                    languageCode: 'en',
                    summary: this.summary,
                    callToAction: {
                        actionType: this.cta_type,
                        url: this.cta_link
                    },
                    alertType: this.type,
                    topicType: "ALERT",
                }
            },
        }
        fetch.method = "POST";
        fetch.url = root_url + '/localPost/newAlert';

        await fetch.execute_fetch()
            .then(response => response.json())
            .then(response => { return response; })
            .catch(error => { alert(error); });
    }
}

export default AlertPost;