class Message_info {
    constructor(title, body, reload = false) {
        this.title  = title;
        this.body   = body;
        this.reload = reload;
    }

    show_message() {
        let msg_container   = document.createElement('DIV');
        let msg_box         = document.createElement('DIV');
        let msg_title       = document.createElement('H2');
        let msg_body        = document.createElement('P');
        let msg_footer      = document.createElement('DIV');

        msg_container.classList.add('msg_container');

        msg_box.classList.add('msg_box');

        msg_title.classList.add('title');
        msg_title.textContent = this.title;

        msg_body.classList.add('msg_body');
        msg_body.innerText = this.body;

        msg_footer.classList.add('msg_footer');
        let exit = document.createElement("SPAN")
        exit.innerText = 'Close';
        exit.classList.add('btn');
        exit.classList.add('btn-primary');
        exit.addEventListener('click', btn_exit => {
            msg_container.remove();
            if(this.reload) { window.location.reload(); }
        });

        msg_footer.appendChild(exit);

        msg_box.appendChild(msg_title);
        msg_box.appendChild(msg_body);
        msg_box.appendChild(msg_footer);
        msg_container.appendChild(msg_box);

        document.querySelector('body').appendChild(msg_container);
    }
}

export default Message_info;