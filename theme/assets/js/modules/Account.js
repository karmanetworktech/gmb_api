import Fetch from "../modules/fetch.js";

class Account {

    constructor(account_id) {
        this.account_id = account_id;
    }

    async get_locations() {
        var fetch = Fetch;

        fetch.url   = root_url + `/account/${this.account_id}/locationsList`;
        fetch.method= 'GET';

        let locations = await fetch.execute_fetch()
            .then(results => results.json())
            .then(list => { return list; })
            .catch(error => { return error; });

        return locations;
    }

    filterOpenLocations(list ) {
        var locationsFilter = [];
        
        if(list) {
            list.forEach(location => {
                if(location.openInfo.status == 'OPEN') {
                    locationsFilter.push(location);
                }
            });
        }

        return locationsFilter;
    }

}

export default Account;