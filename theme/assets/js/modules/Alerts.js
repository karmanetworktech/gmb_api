class Alerts {
    constructor(type, msg, title) {
        this.type   = type;
        this.title  = title;
        this.msg    = msg;
    }

    show_alert = () => {

        let list_alert = document.querySelector('.list-alert-gmb');

        if(!list_alert) {
            list_alert = document.createElement('DIV');
            list_alert.classList.add('list-alert-gmb');

            let main = document.querySelector('main');
            main.append(list_alert);
        }

        let alert = document.createElement('DIV');
        alert.classList.add('gmb_alert');
        alert.classList.add(this.type);
        alert.addEventListener('click', __alert => { window.location.reload(); })
        list_alert.append(alert);

        let title = document.createElement('H2');
        title.classList.add('title');
        title.textContent = this.title;
        alert.append(title);

        if(this.msg.includes('<br>')) {
            let _msg = this.msg.split('<br>');

            _msg.forEach(msg => {
                if(msg != '') {
                    let message = document.createElement('P');
                    message.classList.add('text');
                    message.textContent = msg;
                    alert.append(message);
                }
            })
        }
        else {
            let msg = document.createElement('P');
            msg.classList.add('text');
            msg.textContent = this.msg;
            alert.append(msg);
        }

        setTimeout(() => alert.classList.add('show') ,10);
        setTimeout(() => {
            alert.classList.remove('show')
            setTimeout(() => alert.remove(), 500);
        } ,4500)
    }
}

export default Alerts;