import Account from './Account.js';
import {ElementCreator, Loading} from "./userIteraction.js";

class LocationFilter {
    constructor(account, selector) {
        this.account  = account;
        this.selector = selector;
    }

    listLoctaionSelect = async() => {
        var loading = new Loading();
        loading.addLoading();

        var account_info = new Account();
        account_info.account_id = this.account;
    
        //Buscar as localizações
        var locations = await account_info.get_locations();
        locations = account_info.filterOpenLocations(locations);
    
        //Apagar a lista anterior
        var old_options = this.selector.querySelectorAll('option');
        if(old_options) { old_options.forEach(option => { option.remove() });  }
        
        this.selector.parentElement.querySelector('.selector_value').value = '';
    
        //Gerar a nova listagem do locations
        if(locations.length > 0) {
            var allLocation =  new ElementCreator('OPTION', null, 'value:all', 'All locations', this.selector);
            allLocation.create_element();

            locations.forEach(locale => {
                new ElementCreator('OPTION', null, `value:${locale.name}`, locale.title, this.selector).create_element();
            });
        }

        loading.remove_loading();
    }
}

export default LocationFilter;