export class Loading {

    constructor(counter = false) {
        this.counter = counter;
    }

    addLoading = () => {
        let loading = document.querySelector('.loading');

        if(!loading) {
            let div_loading = document.createElement('DIV');
            let box         = document.createElement('DIV');
            let img         = document.createElement('IMG');

            img.setAttribute('src', 'https://cdn.worldvectorlogo.com/logos/google-my-business-logo.svg');
            div_loading.classList.add('loading');
            box.classList.add('box');

            box.appendChild(img);
            div_loading.appendChild(box);

            document.querySelector('body').prepend(div_loading);

            if(this.counter) {
                let loading_percent = document.createElement('div');
                let loading_doing = document.createElement('div');

                loading_percent.classList.add("percent_anim");

                loading_doing.classList.add('doing');

                setTimeout(() => {
                    loading_doing.style.width = "0%";
                }, 200);

                loading_percent.append(loading_doing);

                box.appendChild(loading_percent);
                div_loading.append(box);
            }

            else {
                let load_anim   = document.createElement('DIV');
                let item_one    = document.createElement('SPAN');
                let item_two    = document.createElement('SPAN');
                let item_three  = document.createElement('SPAN');
                let item_four   = document.createElement('SPAN');

                load_anim.classList.add('load-anim');
                item_one.classList.add('item');
                item_two.classList.add('item');
                item_three.classList.add('item');
                item_four.classList.add('item');

                load_anim.appendChild(item_one);
                load_anim.appendChild(item_two);
                load_anim.appendChild(item_three);
                load_anim.appendChild(item_four);

                box.appendChild(load_anim);
                div_loading.appendChild(box);
            }
        }
    }

    remove_loading = () => {
        let loading = document.querySelector('.loading');

        if(loading) {
            loading.remove();
        }
    };
}


export class ElementCreator{

    constructor(element_html, class_list = null, attributes = null, value = null, destiny = null) {
        this.element_html   = element_html;
        this.class_list     = class_list;
        this.attributes     = attributes;
        this.value          = value;
        this.destiny        = destiny;
    }

    create_element = () => {
        let _element = document.createElement(this.element_html);
        let _class_list = !this.class_list ? null : this.class_list.split(',');
        let _attributes = !this.attributes ? null : this.attributes.split(',');

        if(_class_list) {
            _class_list.forEach(class_item => {
                _element.classList.add(class_item);
            })
        }

        if(_attributes) {
            _attributes.forEach(att => {
                if(att.includes('http:')) { att = att.replace('http:','http|'); }
                if(att.includes('https:')) { att = att.replace('https:','https|'); }

                let attribute = att.split(':');

                if(attribute[1].includes('https|')) { attribute[1] = attribute[1].replace('https|', 'https:') }
                if(attribute[1].includes('http|')) { attribute[1] = attribute[1].replace('http|', 'http:') }
                _element.setAttribute(attribute[0], attribute[1]);
            })
        }

        if(this.value) {
            _element.textContent = this.value;
        }

        if(this.destiny) {
            this.destiny.append(_element);
        } else {
            return _element;
        }
    }
}

export default {Loading, ElementCreator};