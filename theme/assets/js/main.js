    import Fetch                        from '../js/modules/fetch.js';
    import Message_info                 from "../js/modules/message_info.js";
    import {Loading, ElementCreator}    from "../js/modules/userIteraction.js";
    import LocalPost                    from "../js/modules/LocalPost.js";
    import GoogleSheets                 from "../js/modules/GoogleSheets.js";

    function sleep(ms) { return new Promise(resolve => setTimeout(resolve, ms)); }

    const loading = new Loading;
    const element_creator = new ElementCreator;

    const btn_clone_post = document.querySelectorAll('[data-post-clone]');
    if(btn_clone_post) {
        btn_clone_post.forEach(btn => {
            btn.addEventListener('click', btn_clicked => {
                loading.addLoading();

                let local_post = new LocalPost();
                let post_data = btn_clicked.currentTarget;
                post_data = post_data.dataset.postClone.split('/');

                local_post.post_to_clone = {
                    'account' : post_data[1],  'location': post_data[3],  'localPostId' : post_data[5]
                }

                local_post.relaunch_posts()
                    .then(() => {
                        setTimeout(() => window.location.reload(), 2000);
                    });
            });
        });
    }

    const btn_clone_location_post = document.querySelector('#relaunch_location_post');
    if(btn_clone_location_post) {
        btn_clone_location_post.addEventListener('click', async btn_clicked => {

            let account     = btn_clicked.currentTarget.parentElement.parentElement.querySelector('#account');
            let location    = btn_clicked.currentTarget.parentElement.parentElement.querySelector('#location');
            let all_cards = document.querySelectorAll('.cards .localPost');
            let allPostData = [];

            if(all_cards.length > 0) {
                all_cards.forEach(card => {
                    let post_action_info = card.querySelector('[name=action_info]');

                    let post = {
                        languageCode: 'en_US',
                        summary: card.querySelector('.summary').dataset.text,
                        topicType: "STANDARD",
        
                        callToAction: {
                            actionType: post_action_info.dataset.type,
                            url: post_action_info.dataset.url
                        },
        
                        "media": [
                            {
                                "mediaFormat": "PHOTO",
                                "sourceUrl": card.querySelector('img.card-img-top').getAttribute('src')
                            }
                        ]
                    };

                    allPostData.push({
                        post_id: card.dataset.post,
                        post_detail: post
                    });
                });
            }

            console.log(account, location, allPostData);
            
            /*let local_post = new LocalPost();
            local_post.post_list.push({
                account: document.querySelector('#account').value,
                location: document.querySelector('#location').value,
                posts: allPostData
            });

            local_post.relaunch_posts().then(() => {
                setTimeout(() => window.location.reload(), 2000);
            });*/
        });
    }

    const btn_clone_account_post = document.querySelector('#relaunch_account_post');
    if(btn_clone_account_post) {
        btn_clone_account_post.addEventListener('click', btn_clicked => {
            let parentEl = btn_clicked.currentTarget.parentElement;

            let account     = parentEl.querySelector('#account');
            let allPosts    = parentEl.parentElement.querySelectorAll('.cards .card');
            let listPost    = [];

            if(allPosts) {
                allPosts.forEach(async post => {
                    listPost.push({
                        'account': account.value, 'location': post.dataset.location, 'localPostId': post.dataset.post
                    });
                });
            }

            relaunch_multiples_post(listPost);
        });
    }

    const relaunch_multiples_post = async (posts) => {
        let localPost   = new LocalPost;
        loading.addLoading();

        localPost.post_to_clone = posts;
        await localPost.relaunch_posts()
            .then(() => {
                setTimeout(() => window.location.reload(), 2000);
            });
    }

    // *******************************  Reviews ******************************* //
    const list_reviews_btns = document.querySelectorAll('#list_reviews');
    if(list_reviews_btns) {
        list_reviews_btns.forEach(list_reviews_btn => {
            list_reviews_btn.addEventListener('click', async(btn) => {
                let account = btn.currentTarget.dataset.account;
                let location = btn.currentTarget.dataset.location;
                listReviews(account, location);
            });
        })
    }

    const listReviews = async (account, location) => {
        let url = `http://localhost/gmb_api/accounts/${account}/location/${location}/reviews`;
        loading.addLoading()

        let fetch = Fetch;
        fetch.url = url;
        fetch.method = 'GET'

        await fetch.execute_fetch()
            .then(response => response.json())
            .then(response => {
                if(response.totalReviewCount > 0) {
                    list_user_review(document.querySelector('#modal_reviews .modal-body'), response.reviews);
                    $(`#modal_reviews`).modal('show')
                }

                loading.remove_loading()
            })
            .catch(error => {
                loading.remove_loading()
                alert(error)
            });
    }

    const list_user_review = (destiny, review_list) => {
        if(review_list.length > 0) {
            review_list.forEach(review_item => {
                //Create div container
                element_creator.element_html = 'DIV';
                element_creator.class_list = 'user_review';
                let _user_review = element_creator.create_element();
                let test = new ElementCreator('DIV', 'user-reviews', null, null, null).create_element();

                let user_review = document.createElement('DIV');
                let user_img    = document.createElement('IMG');
                let user_name   = document.createElement('h2');
                let user_rating = document.createElement('P');

                user_img.setAttribute('src', review_item.reviewer.profilePhotoUrl);
                user_name.textContent = review_item.reviewer.displayName;
                user_rating.textContent = review_item.starRating;

                user_review.append(user_img);
                user_review.append(user_name);
                user_review.append(user_rating);

                destiny.append(user_review);
            })
        }
    }
    // *******************************  Reviews  ******************************* //

    // *******************************  List Sheets  ******************************* //
    const list_sheets_btn = document.querySelectorAll('.list-sheets');
    const btn_create_post = document.querySelector('#modal_posts_to_create #btn_create_posts');
    let posts_list = null;

    if(list_sheets_btn) {
        list_sheets_btn.forEach(list_sheet => {
            list_sheet.addEventListener('click', async (el) => {
                loading.addLoading();

                let sheet   = new GoogleSheets();
                let destiny = document.querySelector('#modal_sheets');

                account = el.currentTarget.dataset.account;                
                sheet.account = el.currentTarget.dataset.account;
                sheet.location= el.currentTarget.dataset.location;

                await sheet.listAllGoogleSheets(destiny);

                loading.remove_loading();
            });
        })
    }

    if(document.querySelector('#post_sheet_preview')) {
        let item_info = document.querySelector('#post_sheet_preview');
        item_info.addEventListener('click', async () => {
            loading.addLoading();
            let columns = document.querySelectorAll('#modal_sheets_columns_select select');
            let destiny = document.querySelector('#modal_posts_to_create');

            let sheet  = new GoogleSheets();
            await sheet.generatePreview(item_info, columns, destiny, account);
            posts_list = sheet.post_list;

            loading.remove_loading();
        })
    }

    if(btn_create_post) {
        btn_create_post.addEventListener('click', async (btn) => {
            if(!btn.currentTarget.classList.contains('disabled')) {
                loading.addLoading();

                let account = btn.currentTarget.dataset.account;
                let modal_father= btn.currentTarget.parentElement.parentElement;
                let posts = [... posts_list];

                var options = modal_father.querySelector('#locationsList').selectedOptions;
                    options = [... options].filter(value => value.value != 'all');
                    
                if(!options || options.length === 0) { 
                    loading.remove_loading();
                    alert('Select a location from the list!'); 
                    return; 
                }

                var locations = [... options].map(({ value }) => { return value.split('/')[1]; });

                var info = { account, locations, posts };

                await new LocalPost().creatPost(info);

                window.location.reload();
            }
        })
    }

    const create_post = async (account, locations, posts_list) => {

        let list = [];
        let pos = 0;
        for (let i = pos; i < locations.length; i++) { list.push(locations[i]); }

        loading.addLoading();

        let localPost = new LocalPost();
        localPost.account       = account;
        localPost.list_location = list;
        localPost.list_posts    = posts_list;

        let createPost = await localPost.creatMultiplesPost();

        loading.remove_loading();
    }

    //Update filter value
    const filter_option = document.querySelector('#filter_date');
    if(filter_option) {
        filter_option.addEventListener('change', filter => {
            let el_father = filter_option.parentElement;
           if(filter.currentTarget.value == 'today') {
                el_father.classList.remove('col-md-12');
                el_father.classList.add('col-md-6');
                document.querySelector('#date_column').parentElement.classList.remove('hide');
           } else {
               el_father.classList.add('col-md-12');
               el_father.classList.remove('col-md-6');
               document.querySelector('#date_column').parentElement.classList.add('hide');
           }
        });
    }

    // *******************************  List Sheets  ******************************* //

    // *******************************  DELETE LOCALPOST  ******************************* //
    /*const all_btn_post_delete = document.querySelectorAll('.post-delete');
    if(all_btn_post_delete) {

        all_btn_post_delete.forEach(btn_delete => {
           btn_delete.addEventListener('click', button => {
                let parent  = button.currentTarget.parentElement.parentElement;
                let account = parent.dataset.account;
                let location= parent.dataset.location;
                let postId  = parent.dataset.post;

                delete_localPost(account, location, postId);
           });
        });
    }*/

    const delete_localPost = async (account, location, postId) => {

        if (confirm('Would you really like to remove this post?')) {
            loading.addLoading();

            let localPost = new LocalPost();
            localPost.account   = account;
            localPost.location  = location;
            localPost.post_id   = postId;

            let remove_post = await localPost.removeLocalPost();
            window.location.reload();
        }
    }

    //Remove all posts of an locations
    const btn_location_post_delete = document.querySelector('#remove_location_post');
    if(btn_location_post_delete) {
        btn_location_post_delete.addEventListener('click', async button => {
            let all_posts   = document.querySelector('.container.cards');
            let posts       = all_posts.querySelectorAll('.card');

            if(posts.length > 0) {
                loading.addLoading();
                let account = posts[0].dataset.account;
                let location = posts[0].dataset.location;

                let posts_list = [];

                posts.forEach(post => { posts_list.push(post.dataset.post); })

                let localPost = new LocalPost();
                localPost.account   = account;
                localPost.location  = location;
                localPost.list_posts= posts_list;

                let removePostslocation = await localPost.removeAllLocalPostFromLocation();
                window.location.reload();
            }

        });
    }

    // *******************************  DELETE LOCALPOST  ******************************* //

    // *******************************  METRICS LOCALPOST  ******************************* //
    const all_btn_post_metrics = document.querySelectorAll('.post-metrics');
    if(all_btn_post_metrics) {

        all_btn_post_metrics.forEach(btn_metrics => {
            btn_metrics.addEventListener('click', button => {
                let parent  = button.currentTarget.parentElement.parentElement;
                let account = parent.dataset.account;
                let location= parent.dataset.location;
                let postId  = parent.dataset.post;
                let date    = parent.dataset.date;

                metrics_localPost(account, location, postId, date);
            });
        });
    }

    const metrics_localPost = async (account, location, postId) => {
        loading.addLoading();

        let localPost = new LocalPost();
        localPost.account   = account;
        localPost.location  = location;
        localPost.post_id   = postId;

        let post_metrics = await localPost.getMetrics();
    }


    // *******************************  METRICS LOCALPOST  ******************************* //


    $(function () {
        $('[data-toggle="popover"]').popover({
            html: true,
            content: function() {
                return $('#popover-content').html();
            }
        })
    })