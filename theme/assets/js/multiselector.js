class elementCreator {

    constructor(type = 'DIV', attributes = null, classes = null, dataAtributes = null, text = '') {
        this.elementType        = type;
        this.elementAttributes  = attributes;
        this.elementClass       = classes;
        this.elementDataAttr    = dataAtributes;
        this.textValue          = text
    }

    init() {
        let elementCreated = document.createElement(this.elementType);

        if(this.elementClass && this.elementClass.length > 0) {
            this.elementClass.forEach(classe => { elementCreated.classList.add(classe); });
        }

        if(this.elementDataAttr && this.elementDataAttr.length > 0) {
            this.elementDataAttr.forEach(dataAttr => {
                for (let [key, value] of Object.entries(dataAttr)) { elementCreated.dataset[key] = value; }
            });
        }

        if(this.elementAttributes && this.elementAttributes.length > 0) {
            this.elementAttributes.forEach(atribute => {
                for (let [key, value] of Object.entries(atribute)) { elementCreated.setAttribute(key, value); }
            });
        }

        if(this.textValue != '') { elementCreated.textContent = this.textValue; }

        return elementCreated;
    }
}

var multiSelectors  = document.querySelectorAll('.selector-container');
var selectedFields = {};
document.addEventListener("click", removeMultiSelector, false);

if(multiSelectors) {
    multiSelectors.forEach(multiSelect => {
        multiSelect.querySelector('input').disabled = true;
        selectedFields[multiSelect.parentElement.querySelector('select').id] = [];

        if(!multiSelect.querySelector('.arrow')) {
            let arrow = new elementCreator('DIV', null, ['arrow']).init();
            multiSelect.append(arrow);
        }

        multiSelect.addEventListener('click', el => {
            el = el.currentTarget;
            var last_active = document.querySelector('.selector-container.active');

            if(last_active && last_active != el) { last_active.classList.remove('active'); }
            
            let fatherElement   = el.parentElement;
            let selectElement   = fatherElement.querySelector('select');
            let options         = selectElement.querySelectorAll('option');
            
            if(!document.querySelector('#selector_' + selectElement.id)) {
                
                if(el.dataset.click != 'false') {
                    el.dataset.click = 'false';
                    el.classList.add('active');

                    if(document.querySelector('.selector-ui')) { document.querySelector('.selector-ui').remove(); }
                    
                    setTimeout((item = el) => { delete item.dataset.click; }, 600);
                    //create container
                    let container = new elementCreator('DIV', [{'id' : 'selector_' + selectElement.id}], ['selector-ui'], [{'select' : selectElement.id}]);
                    
                    var selectContainer = container.init();
                    fatherElement.append(selectContainer);
                
                    if(options) {
                        let aux = 0;
                        let ul = new elementCreator('ul').init();
                        selectContainer.append(ul);
                
                        let filter = new elementCreator('INPUT', [{'type':'text'}], ['list-filter', 'form-control']).init();
                        filter.addEventListener('input', filterItems);
                        
                        let icon = new elementCreator('I', null, ['fas', 'fa-search']).init();
                        ul.append(filter);
                        ul.append(icon);

                        options.forEach(option => {
                            //create option field
                            let elOption = new elementCreator('LI', null, null, [{'index' : aux}]).init();
                
                            //Element label
                            let label = new elementCreator('LABEL', [{'for' : option.value.replace(' ', '_')}], null, null, option.text).init();
                
                            //Element inout checkbox
                            let input_attr = { type : 'checkbox', 'name' : option.value.replace(' ', '_'), id : option.value.replace(' ', '_'), }
                            if(option.selected) { input_attr.checked = true };
                            let input = new elementCreator('INPUT', [ input_attr ]).init();
                
                            elOption.append(input);
                            elOption.append(label);
                            ul.append(elOption);
        
                            input.addEventListener('change', changeSelectedIndex);
                
                            aux++;
                        });
        
                        setTimeout(() => { ul.classList.add('active'); }, 10);
                    } 
                }

            } else {
                if(el.dataset.click != 'false') {
                    el.dataset.click = 'false'
                    el.classList.remove('active');
                    document.querySelector('#selector_' + selectElement.id + ' ul').classList.remove('active');
                    setTimeout(() => {
                        document.querySelector('#selector_' + selectElement.id).remove();
                        delete el.dataset.click;
                    }, 300);
                }
            }
    
        });
    });
}

function changeSelectedIndex() {
    var element = this;

    //Toggle all selection
    var multiSelector       = this.parentElement.parentElement;
    var multiSelectorItens  = element.parentElement.parentElement.parentElement.parentElement;
    
    if(this.id == 'all') { 
        checkAllLocations(this, multiSelector, multiSelectorItens); 
    } else {
        checkOneLocation(this, multiSelector, multiSelectorItens); 
    }
}

function removeMultiSelector(event) {
    let multiSelector = document.querySelector('.selector-ui');
    let inputSelector = document.querySelectorAll('.selector-container');
    let inputHasClick = false;

    if(inputSelector) {
        inputSelector.forEach(input => {
            if(input.contains(event.target)) 
                inputHasClick = true;
        });
    }

    if(multiSelector) {
        if( !multiSelector.contains(event.target)  && !inputHasClick && document.querySelector('.selector-container.active') ) {
            document.querySelector('.selector-container.active').classList.remove('active');
            document.querySelector('.selector-ui .active').classList.remove('active');
            setTimeout(() => {
                multiSelector.remove();
            }, 300);
        }
    }
}

function checkAllLocations(item, multiSelector, container) {
    var listItens       = multiSelector.querySelectorAll('li input');
    var inputText       = container.querySelector('.selector-container input');
    var containerOPtions= container.querySelectorAll('#locationsList option');

    var check = item.checked;

    if(check) {
        inputText.value = 'All locations'
    } else { 
        inputText.value = '' 
    }

    for (let index = 0; index < listItens.length; index++) {
        containerOPtions[index].selected = check;

        if(listItens[index].id != 'all') {
            listItens[index].checked = check;
            if(!check) { listItens[index].parentElement.classList.remove('selected'); }
            else { listItens[index].parentElement.classList.add('selected'); }
        }
    }
}

function checkOneLocation(item, multiSelector, container) {
    var listItens       = multiSelector.querySelectorAll('li input');
    var inputText       = container.querySelector('.selector-container input');
    var containerOPtions= container.querySelectorAll('#locationsList option');

    let allItensChecked = true;

    //Check the selected items
    if(item.checked) { item.parentElement.classList.remove('selected'); } 
    else { item.parentElement.classList.add('selected'); }

    for (let index = 0; index < listItens.length; index++) {
        if(containerOPtions[index].value == item.id) {
            if(containerOPtions[index].selected) { containerOPtions[index].selected = false; } 
            else { containerOPtions[index].selected = true; }
        }

        if(listItens[index].id !== 'all' && !listItens[index].checked) { allItensChecked = false; }
    }
    
    inputText.value = '';
    listItens.forEach(element => {
        if(element.checked) {
            let text = element.parentElement.querySelector('label').textContent;

            if(inputText.value == '') {
                inputText.value = text;
            } else {
                inputText.value += ', ' + text;
            }
        }
    });

    if(allItensChecked) {
        listItens[0].checked = true;
        listItens[0].parentElement.classList.add('selected');
        containerOPtions[0].selected = true;
        
        inputText.value = 'All locations';
    } else {
        listItens[0].checked = false;
        listItens[0].parentElement.classList.remove('selected');
        containerOPtions[0].selected = false;

        if(inputText.value == 'All locations') {
            inputText.value = '';
        }
    }
}

const filterItems = (el) => {
    var originalText = el.currentTarget.value.toLowerCase();
    var items = el.currentTarget.parentElement.querySelectorAll('li');
    
    items.forEach(item => {
        let itemText = item.textContent.toLowerCase();

        if((!itemText.includes(originalText) || itemText == 'all locations') && originalText.trim() !== '' ) {
            item.classList.add('hide');
        } else {
            item.classList.remove('hide');
        }
    });
}