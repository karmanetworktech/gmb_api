import Fetch from "../modules/fetch.js";
import { ElementCreator, Loading } from "../modules/userIteraction.js";
const getLogs = document.querySelector('#logList');
const fetch = Fetch;

if (getLogs) {
    getLogs.addEventListener('submit', (form) => {
        form.preventDefault();
        getLog();
    });
}

async function getLog() {
    var data = new FormData(getLogs);
    data = {
        itensPerPage: data.get('itensPerPage'),
        logType     : data.get('logType'),
        orderBy     : data.get('orderBy'),
        order       : data.get('order'),
        startDate   : data.get('start_date'),
        finishDate  : data.get('end_date'),
    }

    if (this !== undefined && this.dataset.page) { data.page = this.dataset.page; }

    fetch.url = root_url + '/admin/logs';
    fetch.method = 'POST';
    fetch.body = data;

    await fetch.execute_fetch()
        .then((response) => response.json())
        .then(async data => {
            var table = document.querySelector('#logTable tbody');
            var oldrows = table.querySelectorAll('tr');
            if (oldrows) { oldrows.forEach(row => { row.remove(); }); }

            if(typeof(data.logs) === 'string') {
                new ElementCreator('TR', null, '', data.logs, table).create_element();

            } else {
                data.logs.forEach(log => {
    
                    let row = new ElementCreator('TR').create_element();
    
                    new ElementCreator('TD', null, '', log.date, row).create_element();
                    new ElementCreator('TD', null, '', log.user, row).create_element();
                    new ElementCreator('TD', null, '', log.type_action, row).create_element();
                    new ElementCreator('TD', null, '', log.action, row).create_element();
                    new ElementCreator('TD', null, '', log.action_details, row).create_element();
    
                    table.append(row);
                });
            }

            var pagination = document.querySelector('.pagination');
            var paginteItems = document.querySelectorAll('.pagination .page-item');

            if (this === undefined) {
                for (let index = 1; index <= data.pages; index++) {
                    let classes = index == data.current_page ? 'page-item,active' : 'page-item';

                    let page_item = new ElementCreator('LI', classes, 'data-page:' + (index - 1)).create_element();
                    page_item.addEventListener('click', getLog);

                    let page_link = await new ElementCreator('SPAN', 'page-link', '', index).create_element();

                    page_item.appendChild(page_link);
                    pagination.append(page_item);
                }

                if (paginteItems) {
                    paginteItems.forEach(page => { page.remove(); });
                }
            } else {
                window.scrollTo(0, null);
                if (paginteItems) {
                    paginteItems.forEach(item => {
                        if (item.dataset.page == data.current_page) {
                            item.classList.add('active');
                        } else {
                            item.classList.remove('active');
                        }
                    });
                }
            }
        })
}

const getcsv = document.querySelector('#get_csv');
if (getcsv) {
    getcsv.addEventListener('click', async () => {
        var data = new FormData(getLogs);
        data = {
            itensPerPage: data.get('itensPerPage'),
            logType     : data.get('logType'),
            orderBy     : data.get('orderBy'),
            order       : data.get('order'),
            startDate   : data.get('start_date'),
            finishDate  : data.get('end_date'),
        }

        fetch.url   = root_url + '/admin/logscsv';
        fetch.method= 'POST';
        fetch.body  = data;
        await fetch.execute_fetch()
            .then(response => response.blob())
            .then(response => {
                const blob = new Blob([response], { 
                    type: 'text/csv' 
                });

                const downloadUrl = URL.createObjectURL(blob);
                const a = document.createElement("a");
                a.href = downloadUrl;
                a.download = "kgmb_logs.csv";
                document.body.appendChild(a);
                a.click();
            })
    })
}