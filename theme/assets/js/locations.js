import LocalPost from "../js/modules/LocalPost.js";
import LocationFilter from './modules/LocationFilter.js';
import GoogleSheets from "../js/modules/GoogleSheets.js";
import { Loading } from "../js/modules/userIteraction.js";
import Fetch from '../js/modules/fetch.js';

const loading = new Loading();
// ============================= Remover post de todas as localizações por conta ============================ //
const removeAllPostFromAccounts = document.querySelector('#removeAllPostAllLocations');
if(removeAllPostFromAccounts) {
    removeAllPostFromAccounts.addEventListener('click', async () => {
        var localPost = new LocalPost();
        loading.addLoading();

        var info = { account : account.value, list: [] };
        var allLocations = document.querySelectorAll('.location-posts');

        allLocations.forEach(loc => { 
            let allPosts= loc.querySelectorAll('.card.localPost');
            let posts   = [... allPosts].map(post => post.dataset.post);

            info.list.push({ location: loc.querySelector('[name="location"]').value, posts });
        });

        await localPost.removeLocalPost(info);
        window.location.reload();
    });
}
// ============================= Remover post de todas as localizações por conta ============================ //

/* =============================== Create local post com seleção do tipo de post =============================== //
const postByType = document.querySelector('#createPostByType');
if(postByType) {
    //Ajustar a view de acordo com o tipo de post selecionado
    const postTypeSelector = postByType.querySelector('.modal-header select');
    const postTypeSelectorForms = postByType.querySelectorAll('form');
    
    postTypeSelector.addEventListener('change', (el) => {
        postTypeSelectorForms.forEach(form => {
            if(form.dataset.type == el.currentTarget.value) {
                form.classList.remove('hide');
            } else {
                form.classList.add('hide');
            }
        });
    });
}
// =============================== Create local post com seleção do tipo de post =============================== */

//Página de localizações
// ============================================== Create new post ============================================== //
const createAlert = document.querySelector('#createAlert');
createAlert.addEventListener('submit', async (form) => {
    form.preventDefault();
    loading.addLoading();

    const account = document.querySelector('input[name="account"]').value;
    var options = document.querySelector('#createPostByType #locationsList').selectedOptions;
        options = [... options].filter(value => value.value != 'all');
        
    if(!options || options.length === 0) { 
        loading.remove_loading();
        alert('Select a location from the list!'); 
        return; 
    }

    var locations = [... options].map(({ value }) => { return value.split('/')[1]; });
    var data = new FormData(createAlert);

    var info = {
        account, locations,
        posts: {
            post : {
                languageCode: 'en_US',
                summary     : data.get('postSummary'),
                topicType   : "ALERT",
                alertType   : "COVID_19",
    
                callToAction: {
                    actionType  : 'LEARN_MORE',
                    url         : data.get('actionUrl')
                },
            }
        }
    }

    
    var localPost = new LocalPost();
    await localPost.creatPost(info);
    
    window.location.reload();
});

const createPost = document.querySelector('#createPost');
createPost.addEventListener('submit', async (form) => {
    form.preventDefault();
    loading.addLoading();

    const account = document.querySelector('input[name="account"]').value;
    var options = document.querySelector('#createPostByType #locationsList').selectedOptions;
        options = [... options].filter(value => value.value != 'all');
        
        if(!options || options.length === 0) { 
        loading.remove_loading();
        alert('Select a location from the list!'); 
        return; 
    }

    var locations = [... options].map(({ value }) => { return value.split('/')[1]; });
    var data = new FormData(createPost);

    var info = {
        account, locations,
        posts: {
            post : {
                languageCode: 'en_US',
                summary     : data.get('postSummary'),
                topicType   : "STANDARD",
                callToAction: { actionType: 'LEARN_MORE', url: data.get('actionUrl') },
                media       : [{ mediaFormat: "PHOTO", sourceUrl: data.get('imageUrl'), }]
            }
        }
    }
    
    await new LocalPost().creatPost(info);
    window.location.reload();
});

const createPostFromSheet = document.querySelector('#location_create_posts_from_sheet');
createPostFromSheet.addEventListener('click', async (form) => {
    form.preventDefault();
    loading.addLoading();

    var options = document.querySelector('#modal_posts_to_create #locationsList').selectedOptions;
        options = [... options].filter(value => value.value != 'all');
        
    if(!options || options.length === 0) { 
        loading.remove_loading();
        alert('Select a location from the list!'); 
        return; 
    }

    var locations = [... options].map(({ value }) => { return value.split('/')[1]; });
    var cards = document.querySelectorAll('#modal_posts_to_create .post-item');
    var posts = [];

    if(cards) {
        cards.forEach(card => {
            var post = null;
            if(card.dataset.posttype == 'ALERT') {
                post = {
                    languageCode: 'en_US',
                    summary     : card.querySelector('h2').textContent,
                    topicType   : "ALERT",
                    alertType   : card.dataset.alertType,
        
                    callToAction: {
                        actionType  : card.querySelector('.post-info a').dataset.type,
                        url         : card.querySelector('.post-info a').getAttribute('href')
                    },
                }

            } else {
                 post = {
                    languageCode: 'en_US',
                    summary     : card.querySelector('h2').textContent,
                    topicType   : card.dataset.posttype,
        
                    callToAction: {
                        actionType  : card.querySelector('.post-info a').dataset.type,
                        url         : card.querySelector('.post-info a').getAttribute('href')
                    },
        
                    media : [
                        {
                            mediaFormat : "PHOTO",
                            sourceUrl   : card.querySelector('img').getAttribute('src')
                        }
                    ]
                };
            }

            posts.push(post);
        });
    }

    var info = { account, locations, posts }
    await new LocalPost().creatPost(info);
    window.location.reload();
});

const btnOpenPostModal = document.querySelectorAll('.postPopup');
if(btnOpenPostModal) {
    btnOpenPostModal.forEach(btn => {
        btn.addEventListener('click', (buttonPopup) => {

            var locationsSelect = document.querySelector('#createPostByType #locationsList');
            new LocationFilter(account, locationsSelect).listLoctaionSelect();

            if(buttonPopup.currentTarget.classList.contains('alertPost')) {
                document.querySelector('[data-type="STANDARD"]').classList.add('hide');
                document.querySelector('[data-type="ALERT"]').classList.remove('hide');
                document.querySelector('#createPostByType .modal-header h5').textContent = 'COVID 19 ALERT POST';
            } else {
                document.querySelector('[data-type="STANDARD"]').classList.remove('hide');
                document.querySelector('[data-type="ALERT"]').classList.add('hide');
                document.querySelector('#createPostByType .modal-header h5').textContent = 'DEFAULT POST';
            }
            
            var myModal = new bootstrap.Modal(document.querySelector('#createPostByType'));
            myModal.toggle();
        });
    });
}
// ============================================== Create new post ============================================== //

// ============================================== Relaunch post ============================================== //
const relaunchPostModalOpen = document.querySelector('.posts_relaunch');
if(relaunchPostModalOpen) {
    relaunchPostModalOpen.addEventListener('click', () => {
        var locationsSelect = document.querySelector('#modal_posts_relaunch #locationsList');
        new LocationFilter(account, locationsSelect).listLoctaionSelect();
    })
}

const btnRelaunchPost = document.querySelector('#location_relaunch_posts');
if(btnRelaunchPost) {
    btnRelaunchPost.addEventListener('click', async () => {
        loading.addLoading();
        debugger;
       
        var options = document.querySelector('#modal_posts_relaunch #locationsList').selectedOptions;
        options = [... options].filter(value => value.value != 'all');
            
        if(!options || options.length === 0) { 
            loading.remove_loading();
            alert('Select a location from the list!'); 
            return; 
        }

        var locations = [... options].map(({ value }) => { return value.split('/')[1]; });

        var localPost = new LocalPost();
        await localPost.getPostAccountLocations({ account, locations });

        loading.remove_loading();
    });
}
// ============================================== Relaunch post ============================================== //

const list_sheets_btn = document.querySelector('#location_create_post_sheet');
if (list_sheets_btn) {
    list_sheets_btn.addEventListener('click', async (el) => {
        loading.addLoading();

        let sheet = new GoogleSheets();
        let destiny = document.querySelector('#modal_sheets');

        sheet.account = account;
        await sheet.listAllGoogleSheets(destiny);
        loading.remove_loading();
    });
}

//Location filter
const filter = document.querySelector('#locations_filter');
if(filter) {
    filter.addEventListener('change', (event) => {
        var allLocations = document.querySelectorAll('.locations-list li[data-status]');
        event.preventDefault();

        allLocations.forEach(location => {
            if(event.currentTarget.value === 'ALL') {
                location.classList.remove('hide');
            } else {
                if(location.dataset.status != event.currentTarget.value) {
                    location.classList.add('hide');
                } else {
                    location.classList.remove('hide');
                }
            }
        });
    });
}

//Verifica se as localizações foram listadas
var destiny = document.querySelector('#list_locations .locations-list');
if(!destiny) {
    let url     = root_url + `/account/${account}/locations?page=0`;
    var fetch   = Fetch;
    fetch.url   = url;
    fetch.method= 'GET';

    new Loading().addLoading();

    fetch.execute_fetch()
        .then(result => result.text())
        .then(htmlCode => {
            let destiny = document.querySelector('#list_locations');
            destiny.innerHTML = htmlCode;
            
            var nextPages = destiny.querySelectorAll("#next-link");
            if(nextPages) {
                nextPages.forEach(nextPage => {
                    nextPage.addEventListener('click', (el) => { next_link_click(el); });
                });
            }
            new Loading().remove_loading();
        })
}

const next_link_click = (button) => {
    new Loading().addLoading();
    let page = button.currentTarget.dataset.page;
    let url = button.currentTarget.dataset.action.replace('{account}', account) + '&page=' + page;
    
    var fetch = Fetch;
    fetch.url   = url;
    fetch.method= 'GET';

    fetch.execute_fetch()
        .then(result => result.text())
        .then(htmlCode => {
            let destiny = document.querySelector('#list_locations');
            destiny.innerHTML = htmlCode;

            var nextPages = destiny.querySelectorAll("#next-link");
            if(nextPages) {
                nextPages.forEach(nextPage => {
                    nextPage.addEventListener('click', (el) => { next_link_click(el); });
                });
            }

            new Loading().remove_loading();
        })
}